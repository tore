import common
import Pyro.core, Pyro.errors
import objc

core = Pyro.core.getProxyForURI("PYROLOC://sivad.mine.nu:7766/tord")
sort_by = 2
format = {
    'state': common.fstate,
    'name': str,
    'progress': common.fpcnt,
    'size': common.fsize,
    'download_payload_rate': common.fspeed,
}

def woList(wo):
  return [wo.webScriptValueAtIndex_(i) for i in range(wo.valueForKey_("length"))]

def format_stats(keys, data):
  return [format[k](d) for k,d in zip(keys, data)]

@objc.signature('@@:@i')
def getData(self, wo, sort_by):
  keys = woList(wo)
  data = core.get_torrents_status(None, 'torrent_hash', keys)
  data.sort(lambda x,y: cmp(x[1][sort_by], y[1][sort_by]), reverse=True)
  return [(i, format_stats(keys, s)) for i,s in data]
objc.classAddMethod(objc.lookUpClass('MyPluginClass'), "getData:", getData)

def nameSelector(self, sel): return sel[:-1]
nameSelector = objc.selector(nameSelector, signature="@@::", isClassMethod=1)
objc.classAddMethod(objc.lookUpClass('MyPluginClass'), "webScriptNameForSelector:", nameSelector)
