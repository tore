#import "MyPluginClass.h"

@implementation MyPluginClass

-(id)initWithWebView:(WebView*)w {
    static id _singleton;
    const char *c_path;
    FILE *fp;
    if (_singleton) return _singleton;
    _singleton = self;
	fp = fopen("/Users/Josh/PythonGlue.py", "r");
    Py_Initialize();
    PyRun_SimpleFile(fp, "/Users/Josh/");
    return self;
}

-(void)dealloc {
	[super dealloc];
}

-(void)windowScriptObjectAvailable:(WebScriptObject*)wso {
	[wso setValue:self forKey:@"FortunePlugin"];
}

+(BOOL)isSelectorExcludedFromWebScript:(SEL)sel {
	return NO;
}

+(BOOL)isKeyExcludedFromWebScript:(const char*)k {
	return NO;
}

@end
