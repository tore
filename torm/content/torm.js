var gInfoButton, gDoneButton;
var gFrontHeight, gFrontWidth;

function setup()
{
    gInfoButton = new AppleInfoButton(document.getElementById("infoButton"), document.getElementById("front"), "black", "black", showPrefs);
	gDoneButton = new AppleGlassButton(document.getElementById("doneButton"), "Done", hidePrefs);
  timer()
}

function timer() {
  var keys = new Array("state", "download_payload_rate", "progress", "name");
  var data = FortunePlugin.getData(keys, 1);
  var output = new Array();
  var k = 0;
  
  output[k++] = "<table id='myTable1'><tbody id='myTbody'>";
  for (var i = 0; i < data.length; i++) {
    output[k++] = "<tr class='tr" + (i%2) + "'>";
    var cols = data[i][1];
    for (var j = 0; j < cols.length; j++) {
      output[k++] = "<td>" + cols[j] + "</td>";
    }
    output[k++] = "</tr>";
  }
  output[k++] = "</tbody></table>";
  document.getElementById("middle-middle").innerHTML = output.join("");
  setTimeout("timer()", 1000);
}

function showPrefs()
{
	
	// store the front height and width values to restore on return to front
	gFrontHeight = window.innerHeight;
	gFrontWidth = window.innerWidth;

	
	// Two AppleRect objects store the starting and finshing rectangle sizes
	var startingRect = new AppleRect (0, 0, window.innerWidth, window.innerHeight);
	var finishingRect = new AppleRect (0, 0, 320, 150);
	
	// The RectAnimation specifies the range of values and the handler to call at the animator's interval
	var currentRectAnimation = new AppleRectAnimation( startingRect, finishingRect, rectHandler );

	// The Animator is the timer for the animation
	var currentAnimator = new AppleAnimator (500, 13);
	
	// Associate the animator and animation
	currentAnimator.addAnimation(currentRectAnimation);
	
	// Set a handler to be called when the animation is done (in this case, flip the widget to its back)
	currentAnimator.oncomplete = flipToBack;
	
	// Once everything is set up, start the animation
	currentAnimator.start();
	
}

function flipToBack()
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");
 
	if (window.widget) {
		widget.prepareForTransition("ToBack");
 	}
		
    front.style.display="none";
    back.style.display="block";
 
    if (window.widget){
        setTimeout ('widget.performTransition();', 0);
		widget.setCloseBoxOffset(13,14);
    }
}

// The rectHandler is called each interval by the Animator.  It passes in the animation values at
// this point in the animation.  The values are used to resize parts of the interface and the
// widget as a whole

function rectHandler( rectAnimation, currentRect, startingRect, finishingRect )
{
	document.getElementById("middle").style.height = (currentRect.bottom-40);
	document.getElementById("bottom").style.top = (currentRect.bottom-13);
	window.resizeTo(currentRect.right, currentRect.bottom);
}


function hidePrefs()
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");
	
	if (window.widget) {
		widget.prepareForTransition("ToFront");
 	}

    back.style.display="none";
    front.style.display="block";
	
    if (window.widget) {
        setTimeout ('widget.performTransition();', 0);
		widget.setCloseBoxOffset(7,9);
	}
	
	// Once the flip to the front is complete, flipToFront resizes the widget back to its former size.
	setTimeout("flipToFront()", 750);
		
}


// flipToFront uses another AppleRectAnimation to resize the widget to its previous size

function flipToFront()
{
	var startingRect = new AppleRect (0, 0, 320, 150);
	var finishingRect = new AppleRect (0, 0, gFrontWidth, gFrontHeight);

	var currentRectAnimation = new AppleRectAnimation( startingRect, finishingRect, rectHandler );

	var currentAnimator = new AppleAnimator (500, 13);
	currentAnimator.addAnimation(currentRectAnimation);
	currentAnimator.start();	
}



// Standard widget resize code follows...

var growboxInset;
 
function mouseDown(event)
{
 
    document.addEventListener("mousemove", mouseMove, true);
    document.addEventListener("mouseup", mouseUp, true);
 
    growboxInset = {x:(window.innerWidth - event.x), y:(window.innerHeight - event.y)};
 
    event.stopPropagation();
    event.preventDefault();
}
 
function mouseMove(event)
{
	// checks if the reported event data is legit or not
	if((event.x == -1 ) ) { break; }
	
	var x = event.x + growboxInset.x;
    var y = event.y + growboxInset.y;
 
	if(x < 113) 	// an arbitrary minimum width
		x = 113;

	if(y < 74) 		// an arbitrary minimum height
		y = 74;

	document.getElementById("middle").style.height = (y-30);
	document.getElementById("bottom").style.top = (y-13);
    window.resizeTo(x,y);
 
    event.stopPropagation();
    event.preventDefault();
}
 
function mouseUp(event)
{
    document.removeEventListener("mousemove", mouseMove, true);
    document.removeEventListener("mouseup", mouseUp, true); 
 
    event.stopPropagation();
    event.preventDefault();
}
