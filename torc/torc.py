#!/usr/bin/python
import os.path
import pytx, traceback, sys, logging

from tord.pyro import *
from tord.common import *
from views import *
from list import *

DEFAULT_PREFS = {}

class Msg(pytx.List):
  def __init__(self, flags=0, size=5):
    pytx.List.__init__(self, flags)
    self.size = size
    self.count = 0
  
  @test 
  def msg(self, text):
    self.add(self.count, text)
    self.count += 1;
    self.scroll(1)
    self.draw()

  @test 
  def size_cb(self, s, d):
    return self.size

class Status(pytx.List):
  def __init__(self, core, flags = 0):
    pytx.List.__init__(self, flags)
    self.core = core
    
    self.keys = ["download_rate", "download_limit", "upload_rate", "upload_limit", "num_peers", "dht"]
    self.format = [fspeed, fspeed, fspeed, fspeed, fpeers, lambda x: x<0 and "off" or fpeers(x)]

  @test 
  def idle_cb(self):
    data = self.core.get_session_status(self.keys)
    status = [f(v) for k, f, v in zip(self.keys, self.format, data)]
    text = "Download[%6s|%6s] Upload[%6s|%6s] Peers[%3s] DHT[%3s]" % tuple(status)
    self.add(0, text)
    self.draw()

  @test 
  def size_cb(self, s, d):
    return 1

class TXTor:
  def __init__(self):
    pytx.init()
  
  @test 
  def go(self):

    self.config = Config("torc.conf", DEFAULT_PREFS)
    self.core = self.get_core()
    

    l = Views(pytx.FOCUS|pytx.SCROLL|pytx.SEPARATOR|pytx.TITLES|pytx.HIGHLIGHT, 6)
    l.view_add(Torrents(self.core))
    l.view_add(Files(self.core))
    l.view_add(Peers(self.core))

    b = pytx.Box(pytx.VERT, pytx.ROOT|pytx.BORDER)
    m = Msg()
    s = Status(self.core)
    
    b.add(l); b.add(m); b.add(s)
    pytx.adjust()
    
    stream=FakeIO(m.msg, m.draw)
    logging.root.handlers[0].stream = stream
    
    s.idle_cb()
    b.draw()
    l.refresh(update=True)
    
    self.events = {
      "update": lambda c,i,m: l.refresh(update=True, sort=False, args=(c,i)),
      "sort": lambda c,i,m: l.refresh(update=True, sort=True, args=(c,i)),
      "remove": lambda c,i,m: l.refresh(remove=i),
    }
    
    pytx.operate()
    self.looping = False
  
  @test
  def message(self, msg):
    if msg is None:
      self.looping = False
      print "hello"
    subject, alert = msg
    try: self.events[subject](self.get_core(), *alert)
    except: log.info("%-20s %s" % (str(subject)+':', alert[1]))
  
  @test
  def shutdown(self):
    pytx.free()
    logging.root.handlers[0].stream = sys.stderr

def main():
  host = None
  if len(sys.argv) == 2: host = sys.argv[1]
  
  PyroClient(TXTor, host)


