from tord.common import test, log, timer
import pytx

class Column:
  def __init__(self, title, func, width, align):
    self.func = func
    self.title = title
    self.width = width
    self.align = align
  
  @test
  def get_width(self, show_title):
    if show_title and len(self.title) > self.width: return len(self.title)
    return self.width

  @test
  def format_data(self, data, width):
      return self.align(self.func(data), width)

  @test
  def format_title(self, width):
    return self.align(self.title, width)

class Columns:
  def __init__(self):
    self.selected = None
    self.cols = {}

  @test
  def add_column(self, key, column):
    self.cols[key] = column

  def swap_columns(self, a, b):
    self.cols[a], self.cols[b] = self.cols[b], self.cols[a]
  
  @test
  def get_widths(self, show_title):
    return [self.cols[k].get_width(show_title) for k in self.keys]
  
  @test
  def get_titles(self, widths):
    return [self.cols[k].format_title(w) for k, w in zip(self.keys, widths)]
  
  @test
  def format_data(self, data, widths):
    return [self.cols[k].format_data(d,w) for d, w, k in zip(data, widths, self.keys)]
    
class Views(pytx.List):
  def __init__(self, flags, chop):
    pytx.List.__init__(self, flags)
    self.chop = chop
    
    self.views = {}
    self.view = self.root = None
  
  @test
  def view_add(self, view):
    self.views[view.name] = view
    if len(self.views) == 1:
      self.view = self.root = self.views[view.name]

  @test
  def change_view(self, key, selected, store = False):
    if store: self.view.restall = self.view.get_id(store), self.sort_get()
    
    if key: self.view = self.views[key]
    else: self.view = self.root
    
    if selected: self.view.selected = selected
    self.refresh(reset=True, clear=True, update=True, restall=self.view.restall)
  
  @test
  def refresh(self, remove=[], update=False, restall = False, clear=False, reset=False, sync=False, sort=True, args=None):
    for h in remove: self.remove(self.view.get_id(h))
    if clear: self.clear()
    if reset:
      self.reset()
      self.sort_set(0,0)
    if sync or reset: self.sync()
    if update: self.update(self.view.get_data(args))
    if restall:
      selected, sort = restall
      self.selected(selected)
      self.sort_set(*sort)
    if sort: self.sort()
    self.draw()
  
  @test
  def update(self, data):
    for id, row in data:
      self.add(self.view.get_id(id), row)

  @test
  def sorting(self, by, max):
    col, asc = self.sort_get()
    if 0 < by < max: col = by-1
    if not by: asc = not asc
    self.sort_set(col, asc)
    self.refresh()
  
  @test
  def text_cb(self, data):
    return self.view.format_data(data, self.widths)

  @test
  def bind_cb(self, ch, id):
    try: self.view.bindings[chr(ch)](self.view.get_hash(id), self)
    except: self.sorting(ch-48, len(self.view.keys) + 1)
  
  @test
  def idle_cb(self):
    self.refresh(update=True)

  @test
  def rsze_cb(self):
    widths = self.view.get_widths(self.flag_get(pytx.TITLES))
    self.widths_set(widths)
    self.titles_set(self.view.get_titles(widths))
  
  @test
  def size_cb(self, s, d):
    return d - self.chop
