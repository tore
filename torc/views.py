from tord.common import *
from list import Columns, Column

class Base:
  def __init__(self):
    self.ids = {}
    self.hashs = {}
    self.bindings = {}
    self.restall = False

  @test
  def get_id(self, hash):
    if hash not in self.ids:
      self.ids[hash] = len(self.ids)
      self.hashs[self.ids[hash]] = hash
    return self.ids[hash]

  @test
  def get_hash(self, id):
    return self.hashs[id]
  
  @test
  def get_args(self, args):
    core, ids = self.core, self.selected
    if args:
      new_core, new_ids = args
      if new_core: core = new_core
      if new_ids: ids = new_ids
    return core, ids

class Torrents(Columns, Base):
  def __init__(self, core):
    Columns.__init__(self)
    Base.__init__(self)

    self.name = 'Torrents'
    self.core = core
    
    self.keys = ['state', 'download_payload_rate', 'upload_payload_rate', 'progress', 'eta', 'total_wanted', 'num_peers', 'all_time_upload', 'name']

    self.add_column('state',                 Column("Status",   fstate, 7,  aright))
    self.add_column('download_payload_rate', Column('D Rate',   fspeed, 7,  aright))
    self.add_column('upload_payload_rate',   Column('U Rate',   fspeed, 7,  aright))
    self.add_column('all_time_upload',       Column('U Total', fspeed, 7,  aright))
    self.add_column('all_time_download',     Column('D Total', fspeed, 7,  aright))
    self.add_column('progress',              Column("Progress", fbar,   10, aright))
    self.add_column('_progress',             Column("Progress", fpcnt,  8,  aright))
    self.add_column('ratio',                 Column("Ratio",    fratio, 5,  aright))
    self.add_column('eta',                   Column("ETA",      ftime,  7,  aright))
    self.add_column('total_wanted',          Column("Size",     fsize,  6,  aright))
    self.add_column('num_peers',             Column("Peers",    str,    3,  aright))
    self.add_column('name',                  Column('Name',     str,    0,  aleft))
    self.add_column('torrent_hash',          Column('Hash',     str,    0,  aleft))
    #self.add_column('peers_flags',            Column('Flags',    str,    0,  aright))
    #self.add_column('peers_ip',               Column('Flags',    ip, 0, aright))

    self.bindings['b'] = lambda x,l: log.info(core.get_torrents_status([x], 'torrent_hash', ['torrent_file']))
    self.bindings['n'] = lambda x,l: log.info(x)

    self.bindings['f'] = lambda x,l: l.change_view('Files', [x], x)
    self.bindings['l'] = lambda x,l: l.change_view('Peers', [x], x)
    self.bindings['F'] = lambda x,l: l.change_view('Files', None, x)
    self.bindings['L'] = lambda x,l: l.change_view('Peers', None, x)
    self.bindings['y'] = lambda x,l: self.swap_columns('_progress', 'progress') or l.refresh(reset=True, sync=True)

    self.bindings['A'] = lambda x,l: core.set_torrents_state([x], 'queue_position_top', publish='update', all=True)
    self.bindings['Z'] = lambda x,l: core.set_torrents_state([x], 'queue_position_bottom', publish='update', all=True)
    self.bindings['a'] = lambda x,l: core.set_torrents_state([x], 'queue_position_up', publish='sort', all=True)
    self.bindings['z'] = lambda x,l: core.set_torrents_state([x], 'queue_position_down', publish='sort', all=True)
    

    
    self.bindings['R'] = lambda x,l: core.set_torrents_state(None, 'resume')
    self.bindings['P'] = lambda x,l: core.set_torrents_state(None, 'pause')
    self.bindings['t'] = lambda x,l: core.set_torrents_state([x], 'toggle_pause')

    self.bindings['W'] = lambda x,l: core.set_torrents_state(None, 'fastresume_write')
    self.bindings['w'] = lambda x,l: core.set_torrents_state([x], 'fastresume_write')

    self.bindings['i'] = lambda x,l: core.set_torrents_state([x], 'force_reannounce')
    self.bindings['h'] = lambda x,l: core.set_torrents_state([x], 'recheck', publish='update')
    self.bindings['H'] = lambda x,l: core.set_torrents_state(None, 'force_recheck', publish='update', all=True)
    
    self.bindings['D'] = lambda x,l: core.remove(x, True)
    self.bindings['d'] = lambda x,l: core.remove(x)
    
    self.bindings['x'] = lambda x,l: core.set_torrents_state(None, 'set_sequential_download', publish='update', msg=tuple([True]))
    self.bindings['X'] = lambda x,l: core.set_torrents_state(None, 'set_sequential_download', publish='update', msg=tuple([False]))
    
    self.bindings['<'] = lambda x,l: core.set_torrents_state(None, 'toggle_managed', publish='update', msg=tuple([True]))
    self.bindings['>'] = lambda x,l: core.set_torrents_state(None, 'toggle_managed', publish='update', msg=tuple([False]))
    self.bindings['m'] = lambda x,l: core.set_torrents_state([x], 'toggle_managed', publish='update')
    
    self.bindings['Q'] = lambda x,l: core.killServer()
  
  @test
  def get_data(self, args):
    c, i = self.get_args(args)
    return c.get_torrents_status(i, 'torrent_hash', self.keys)

class Files(Columns, Base):
  def __init__(self, core):
    Columns.__init__(self)
    Base.__init__(self)
    
    self.name = "Files"
    self.core = core

    self.keys = ['files_priority', 'files_size', 'files_progress', 'files_path']
    
    def percent(a): return fpcnt(float(a[0])/a[1])
    def bar(a): return fbar(float(a[0])/a[1])

    self.add_column('files_priority',        Column("Priority", str,     1,  aright))
    self.add_column('files_size',            Column("Size",     fsize,   6,  aright))
    self.add_column('files_progress',        Column("Progress", percent, 9, aleft))
    self.add_column('_files_progress',       Column("Progress", bar,     5, aright))
    self.add_column('files_path',            Column('Name',     str,     0,  aleft))
   
    self.bindings['f'] = lambda x,l: l.change_view('Torrents', None)
    self.bindings['y'] = lambda x,l: self.swap_columns('_files_progress', 'files_progress') or l.refresh(reset=True, sync=True)
    self.bindings['-'] = lambda (t,f),l: core.set_torrents_state([t], 'prioritize', publish='update', msg=(f,-1))
    self.bindings['='] = lambda (t,f),l: core.set_torrents_state([t], 'prioritize', publish='update', msg=(f,1))

  def get_data(self, args):
    files = []
    c, i = self.get_args(args)
    for id, data in c.get_torrents_status(i, 'files_id', self.keys):
      files += [((h,i), [d[i] for d in data]) for h, i in id] 
    return files

class Peers(Columns, Base):
  def __init__(self, core):
    Columns.__init__(self)
    Base.__init__(self)
    
    self.name = "Peers"
    self.core = core

    self.keys = ['peers_ip', 'peers_read_state', 'peers_write_state', 'peers_flags', 'peers_source_flags', 'peers_up_speed', 'peers_down_speed', 'peers_payload_up_speed', 'peers_payload_down_speed', 'peers_download_queue_length', 'peers_upload_queue_length']

    def ip(x): return x[0]
    
    def state(x):
      s = ['idle','torrent', 'global', 'network']
      return s[ord(x)]

    def flags(x):
      t = ['I', 'C', 'i', 'c', 'e', 'l', 'H', 'C', 'Q', 'P', 's', 'O', 'S']
      f = ['.', '.', '.', '.', '.', 'r', '.', '.', '.', '.', '.', '.', '.']
      return "".join([((1<<i & x) and t[i]) or f[i] for i in range(len(t))])
    
    def source(x):
      s = ['t', 'd', 'p', 'l', 'r', 'i']
      return "".join([((1<<i & x) and s[i]) or ' ' for i in range(len(s))])
    
    self.add_column('peers_ip',                     Column('Address',  ip,    15,  aleft))
    self.add_column('peers_read_state',             Column("R State",  state,  7,  aleft))
    self.add_column('peers_write_state',            Column("W State",  state,  7,  aleft))
    self.add_column('peers_flags',                  Column("Flags",    flags,  13, aleft))
    self.add_column('peers_source_flags',           Column("Source",   source, 6,  aleft))
    self.add_column('peers_up_speed',               Column("U Speed",  fspeed, 7,  aleft))
    self.add_column('peers_down_speed',             Column("D Speed",  fspeed, 7,  aleft))
    self.add_column('peers_payload_up_speed',       Column("PU Speed", fspeed, 7,  aleft))
    self.add_column('peers_payload_down_speed',     Column("PD Speed", fspeed, 7,  aleft))
    self.add_column('peers_download_queue_length',  Column("D Queue",  str,    7,  aleft))
    self.add_column('peers_upload_queue_length',    Column("U Queue",  str,    7,  aleft))
   
    self.bindings['l'] = lambda x,l: l.change_view('Torrents', None)
    self.bindings['L'] = lambda x,l: l.change_view('Torrents', None)
  
  @test
  def get_data(self, args):
    peers = []
    c, i = self.get_args(args)
    try:
      for ids, ds in c.get_torrents_status(i, 'peers_ip', self.keys):
        for n, i in enumerate(ids):
          peers += [(i,[d[n] for d in ds])]
    except:
      return []
    
    return peers

