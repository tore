import libtorrent as lt
from os import listdir, path
from pyro import PyroServer
from common import *
from shutil import move
from time import sleep

DEFAULT_PREFS = {
    "dht": True,
    "lsd": True,
    "upnp": True,
    "utpex": True,
    "natpmp": False,
    'session': (8, 5, True),
    "encryption": (1,1,2,True),
    "max_upload_speed": 60,
    "max_download_speed": -1,
    "max_connections_global": -1,
    "max_upload_slots_global": -1,
    "listen_ports": (10000, 10100),
    "max_connections_per_torrent": -1,
    "max_upload_slots_per_torrent": -1,
    "path_watched": get_default_dir("downloads/watched"),
    "path_current": get_default_dir("downloads/current/"),
    "path_resume": get_default_dir("downloads/resume/"),
    "path_finished": get_default_dir("downloads/finished"),
    "path_incomplete": get_default_dir("downloads/incomplete"),
}

class Watched:
  @test
  def __init__(self, dir, func):
    try:
      from pyinotify import WatchManager, ThreadedNotifier, EventsCodes, ProcessEvent
      class Action(ProcessEvent):
        def __init__(self, func):
          self.func = func

        def process_IN_CLOSE_WRITE(self, event):
          self.func(event.path, event.name)
    except: return
    self.wm = WatchManager()
    mask = EventsCodes.IN_CLOSE_WRITE | EventsCodes.IN_DELETE
    self.notifier = ThreadedNotifier(self.wm, Action(func))
    self.notifier.start()
    self.wdd = self.wm.add_watch(dir, mask, rec=True)

  def watched_shutdown(self):
    try: self.notifier.stop()
    except: pass

class Alerts:
  def __init__(self, general_cb):
    log.debug("Alerts initialized..")
    self.alert_handlers = {}
    self.general_cb = general_cb
  
  @test
  def alert_register(self, alert_type, handler):
    log.debug("Registering handler for alert %s", alert_type)
    if alert_type not in self.alert_handlers.keys():
      self.alert_handlers[alert_type] = []
    self.alert_handlers[alert_type].append(handler)
  
  @test
  def alert_deregister(self, alert_type, handler):
    log.debug("Deregistering handler for alert %s", alert_type)
    self.alert_handlers[alert_type].remove(handler)

  def alert_handle(self, alert_type, alert):
    log.info("%s: %s" % (alert_type, alert))
    if alert_type in self.alert_handlers.keys():
      for handler in self.alert_handlers[alert_type]:
        handler(alert_type, alert)
      return
    self.general_cb(alert_type, alert)

class Torrent:
  def __init__(self, config, torrent_dir, defaults):
    self.config = config
    self.resume = None
    self.files_id, self.files_path, self.files_size, self.files_offset = [], [], [], []
    
    p = path.join(torrent_dir, defaults['tfile'])
    handle = open(p,'rb')
    data = lt.bdecode(handle.read())

    self.info = lt.torrent_info(data)
    self.hash = str(self.info.info_hash())
    self.name = self.info.name()
    
    defaults['priorities'] = defaults['priorities'] * self.info.num_files()
    self.fastresume_read(defaults)
    self['tpath'] = torrent_dir

    self.generate_files()

  def __getitem__(self, key):
    try: return self.resume[key]
    except: return None

  def __setitem__(self, key, value):
    self.resume[key] = value
 
  @test 
  def fastresume_read(self, default):
    log.info("Reading resume: %s", self.hash)
    try:
      resume_path = path.join(self.config['path_resume'], self.hash)
      resume_handle = open(resume_path, "rb")
      self.resume = lt.bdecode(resume_handle.read())
    except:
      log.error("Reading resume: %s", self.hash)
      self.resume = default
  
  @test 
  def fastresume_write(self):
    
    if not (self.handle.is_valid() and self.handle.has_metadata()):
      log.error("Writing resume (invalid): %s", self.hash)
      return

    if self.handle.status().state == 0:
      log.error("Writing resume (hashing queue): %s", self.hash)
      return
    
    try: 
      resume = self.handle.write_resume_data()
      resume['paused'] = self.handle.status().paused
      resume['managed'] = self.handle.is_auto_managed()
      resume['spath'] = self.handle.save_path()
      resume['tpath'] = self['tpath']
      resume['tfile'] = self['tfile']
      resume['priorities'] = self['priorities']
      
      resume_data = lt.bencode(resume)
      resume_path = path.join(self.config['path_resume'], self.hash)
      open(resume_path, "wb").write(resume_data)
      log.info("Writing resume: %s peers: %d", self.hash, len(resume['peers']))
    except: 
      log.error("Writing resume: %s", self.hash)

  @test
  def fix_download(self):
    s = self.handle.status()
    if s.progress == 1 and self['total_downloaded'] < s.total_wanted:
      self['total_downloaded'] = s.total_wanted
  
  @test
  def get_status(self, id_field, fields):
    s = self.handle.status()
    
    def ratio(up, down):
      if down == 0: return 0
      return float(up) / down
    
    def eta(size, done, speed):
      if (size - done) == 0: return 0
      if speed == 0: return 0
      return (size - done) / speed
    
    def state():
      p = self.handle.queue_position()
      m = self.handle.is_auto_managed()
      return p, m, int(s.state), s.paused
    
    peers = [] 
    def peers_stat(s, ps):
      if not ps: ps = self.handle.get_peer_info()
      return [getattr(p,s) for p in ps]
    
    values = {
        'eta': eta(s.total_wanted, s.total_done, s.download_payload_rate),
        'ratio': ratio(s.all_time_upload, s.all_time_download),
        'name': self.name,
        'torrent_hash': self.hash,
        'files_path': self.files_path,
        'files_size': self.files_size,
        'files_id': self.files_id,
        'files_priority': self['priorities'],
        'torrent_file': self['tfile']
    }

    funcs = {
        'files_progress': lambda: zip(self.handle.file_progress(), self.files_size),
        'peers_flags': lambda: peers_stat('flags', peers),
        'peers_source_flags': lambda: peers_stat('source', peers),
        'peers_read_state': lambda: peers_stat('read_state', peers),
        'peers_write_state': lambda: peers_stat('write_state', peers),
        'peers_upload_queue_length': lambda: peers_stat('upload_queue_length', peers),
        'peers_download_queue_length': lambda: peers_stat('download_queue_length', peers),
        'peers_up_speed': lambda: peers_stat('up_speed', peers),
        'peers_down_speed': lambda: peers_stat('down_speed', peers),
        'peers_payload_up_speed': lambda: peers_stat('payload_up_speed', peers),
        'peers_payload_down_speed': lambda: peers_stat('payload_down_speed', peers),
        'peers_ip': lambda: peers_stat('ip', peers),
        'state': state,
    }
    
    data = [get_stat(s, f, values, funcs) for f in fields]
    id = get_stat(s, id_field, values, funcs)
    return (id, data)

  @test
  def set_state(self, attr, msg):
    try: getattr(self.handle, attr)(*msg)
    except AttributeError: getattr(self, attr)(*msg)
    except: log.error("Not a valid state change method " + attr)

  @test
  def toggle_pause(self):
    if self.handle.status().paused: self.handle.resume()
    else: self.handle.pause()
  
  @test
  def toggle_managed(self, manage=None):
    if manage == None:
      if self.handle.is_auto_managed(): manage = False
      else: manage = True
    self.handle.auto_managed(manage)

  def prioritize(self, file=None, delta=0):
    if file is not None:
      self['priorities'][file] = (self['priorities'][file] + delta) % 10
    self.handle.prioritize_files(self['priorities'])

  def move(self, sp=None, tp=None):
    if sp and sp != self.handle.save_path():
      self.handle.move_storage(sp)

    if tp and tp != self['tpath']:
      move(path.join(self['tpath'], self['tfile']), path.join(tp, self['tfile']))
      self['tpath'] = tp
    
    self.fastresume_write()

  def recheck(self):
    self.handle.force_recheck()
    self.fastresume_write()
  
  def generate_files(self):
    for id, f in enumerate(self.info.files()):
      self.files_id += [(self.hash, id)]
      self.files_path += [f.path]
      self.files_size += [f.size]
      self.files_offset += [f.offset]
  
  def checked_cb(self):
    if self.handle.status().progress == 1: self.finished_cb()
    else: self.move(self.config["path_current"], self.config['path_watched'])

  def finished_cb(self):
    path = self.config["path_finished"]
    self.move(path, path)

class Core(Alerts, Watched):
  def __init__(self):
    self.torrents = {}
    self.config = Config("tord.conf", DEFAULT_PREFS)
    self.config["path_removed"] = "/mnt/data/downloads/removed"

    self.session = lt.session(lt.fingerprint("UT", 1,7,7,0))
    self.session.set_severity_level(lt.alert.severity_levels.info)


    self.session.add_extension(lt.create_metadata_plugin)
    self.session.add_extension(lt.create_ut_metadata_plugin)
    self.session.add_extension(lt.create_smart_ban_plugin)

    def speed(x):
      if x > 0: return x*1000
      return x
    
    self.config["lsd"] = True
    self.config.register_set_function("utpex", lambda k,v: self.session.add_extension(lt.create_ut_pex_plugin))
    self.config.register_set_function("listen_ports", lambda k,v: self.session.listen_on(*v))
    self.config.register_set_function("max_upload_speed", lambda k,v: self.session.set_upload_rate_limit(speed(v)))
    self.config.register_set_function("max_download_speed", lambda k,v: self.session.set_download_rate_limit(speed(v)))
    self.config.register_set_function("max_connections_global", lambda k,v: self.session.set_max_connections(v))
    self.config.register_set_function("max_upload_slots_global", lambda k,v: self.session.set_max_uploads(v))
    self.config.register_set_function("dht", toggle(self.session.start_dht, self.session.stop_dht, (None,)))
    self.config.register_set_function("lsd", toggle(self.session.start_lsd, self.session.stop_lsd))
    self.config.register_set_function("upnp", toggle(self.session.start_upnp, self.session.stop_upnp))
    self.config.register_set_function("natpmp", toggle(self.session.start_natpmp, self.session.stop_natpmp))
    self.config.register_set_function("encryption", self.set_encryption)
    self.config.register_set_function("session", self.set_session)
    self.session.set_max_uploads(4)
    self.session.set_max_half_open_connections(50)


    Alerts.__init__(self, self.alert_cb())
    Watched.__init__(self, self.config['path_watched'], self.add)
    self.alert_register("torrent_paused_alert",   self.alert_cb(lambda x: "update"))
    self.alert_register("torrent_resumed_alert",  self.alert_cb(lambda x: "update"))
    self.alert_register("torrent_finished_alert", lambda t,a: self.set_torrents_state(a[0], 'finished_cb', publish='update'))
    self.alert_register("torrent_checked_alert", lambda t,a: self.set_torrents_state(a[0], 'checked_cb', publish='update'))
    
    #s = lt.session_settings()
    #s.active_downloads, s.active_seeds, s.lazy_bitfields = 10, 1000, 1
    #self.session.set_settings(s)
  
  def go(self):
    log.info("Strarting alert checking loop")
    self.add(self.config['path_watched'])
    self.add(self.config['path_finished'])
    while self.looping:
      self.check_alerts()
      sleep(0.1)

    del self.session
  
  def shutdown(self):
    log.info("Shutting down")
    self.watched_shutdown()
    for t in self.torrents.values(): t.fastresume_write()

  def check_alerts(self):
    alert = self.session.pop_alert()
    while alert:
      alert_type = str(type(alert)).split("'")[1].split(".")[1]
      try: id = str(alert.handle.info_hash())
      except: id = None
      if id: self.alert_handle(alert_type, ([id], alert.message()))
      else: self.alert_handle(alert_type, (str(alert), alert.message()))
      alert = self.session.pop_alert()

  @test
  def add(self, torrent_dir, torrent_file = None):
    def add(torrent_dir, torrent_file):
      log.info("Adding torrent: %s", torrent_file)
      
      default = {
          'spath': self.config["path_current"],
          'tfile': torrent_file,
          'paused': False,
          'managed': True,
          'priorities': [1],
      }
      
      torrent = Torrent(self.config, torrent_dir, default)
      #resume = torrent.resume
      #if len(torrent.resume) == len(default): resume = None
      resume = ""
      try:
        resume = open(path.join(self.config['path_resume'], torrent.hash)).read()
      except:
        log.error("reading resume")

      param = {
          'ti': torrent.info,
          'duplicate_is_error': True,
          'resume_data': resume,
          'save_path': torrent['spath'],
          'paused': torrent['paused'],
          'auto_managed': torrent['managed'],
          'storage_mode': lt.storage_mode_t(1)
      }
      try:
        torrent.handle = self.session.add_torrent(param)
        self.torrents[torrent.hash] = torrent 
      
        torrent.handle.set_max_connections(-1)
        torrent.prioritize()
        torrent.fix_download()
      except:
        log.error("Adding torrent: %s", torrent_file)
    
    ''' add a directory '''
    if not torrent_file:
      log.info("adding torrents in directory: %s", torrent_dir)
      for f in listdir(torrent_dir):
        if f[-8:] == '.torrent': add(torrent_dir, f)
      return
    
    ''' add a single file '''
    if torrent_file[-8:] == '.torrent': add(torrent_dir, torrent_file)
  
  def remove(self, torrent_hash, force = False):
    torrent = self.torrents[torrent_hash]
    log.info("Removing torrent: %s", torrent.hash)
    
    if torrent.handle.status().progress != 1 and not force:
      log.error("Removing torrent: %s", torrent.hash)
      return
    
    if torrent.handle.status().progress != 1: dir = self.config['path_incomplete']
    else: dir = self.config['path_removed']
    
    torrent.move(dir, dir)
    
    self.session.remove_torrent(torrent.handle, 0)
    self.publish(('remove', ([torrent_hash], 'remove')))
    del self.torrents[torrent.hash]
  
  @test
  def get_torrents_status(self, torrents, id_field, fields):
    return [self.torrents[t].get_status(id_field, fields) for t in torrents or self.torrents.keys()]
  
  def get_session_status(self, fields):
    s = self.session.status()
    values = {
        "download_limit": int(self.session.download_rate_limit()),
        "upload_limit": int(self.session.upload_rate_limit()),
        "dht": (self.config["dht"] and s.dht_nodes) or -1
        }
    return [get_stat(s, f, values) for f in fields]
  
  @test 
  def set_torrents_state(self, torrents, attr, msg=(), publish=False, all=False):
    torrents = torrents or self.torrents.keys()
    for t in torrents: self.torrents[t].set_state(attr, msg)
    if publish and all: torrents = None
    self.publish((publish, (torrents, attr)))
 
  def set_encryption(self, key, value):
    o, i, l, r = value
    e = lt.pe_settings()
    e.out_enc_policy = lt.enc_policy(o)
    e.in_enc_policy = lt.enc_policy(i)
    e.allowed_enc_level = lt.enc_level(l)
    e.prefer_rc4 = r
    self.session.set_pe_settings(e)

  def set_session(self, key, value):
    s = lt.session_settings()
    s.dont_count_slow_torrents = 1
    s.active_limit = 5
    s.active_downloads, s.active_seeds, s.lazy_bitfields = value
    self.session.set_settings(s)
  
  def alert_cb(self, subject_fn=lambda x:x):
    return lambda t,a: self.publish((subject_fn(t), a))

def main():
  PyroServer(Core, "tord")
