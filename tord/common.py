import os
import pickle
from time import time as clock
import sys
import logging
import traceback

log_format = "[%(levelname)-8s] %(name)s:%(module)s:%(lineno)d %(message)s"
logging.basicConfig(level=logging.DEBUG, format=log_format, stream=sys.stderr)
log = logging.getLogger()

def timer(f):
  def wrapper(*args, **kwargs):
    s = clock()
    r = f(*args, **kwargs)
    log.info("timing %s: %s" % (str(f), str(clock()-s)))
    return r
  return wrapper

def test(f):
  def wrapper(*args, **kwargs):
    try: return f(*args, **kwargs)
    except: 
      traceback.print_exc()
      log.error("error: " + str(f))
  return wrapper

class FakeIO:
  def __init__(self, writer, flusher = lambda: None):
    self.writer = writer
    self.flusher = flusher

  def write(self, s): self.writer(s)
  def flush(self): self.flusher()

class Config:
  def __init__(self, filename, defaults=None):
      self.config = {}
      self.set_functions = {}
      if defaults: self.config = defaults
      self.config_file = get_config_dir(filename)
      self.load(self.config_file)
      self.save()
      
  def __del__(self):
      self.save()
  
  def __getitem__(self, key):
      return self.config[key]

  def __setitem__(self, key, value):
      self.config[key] = value

  def items(self):
    return self.config.items()

  def get_config(self):
    return self.config
          
  def load(self, filename=None):
      if filename is None:
          filename = self.config_file
      try:
          print filename
          pkl_file = open(filename, "rb")
          filedump = pickle.load(pkl_file)
          self.config.update(filedump)
          pkl_file.close()
      except IOError:
          log.error("IOError: Unable to load file '%s'", filename)
      except EOFError:
          pkl_file.close()
          
  def save(self, filename=None):
      if filename is None:
          filename = self.config_file
      try:
          pkl_file = open(filename, "wb")
          pickle.dump(self.config, pkl_file)
          pkl_file.close()
      except IOError:
          log.error("IOError: Unable to save file '%s'", filename)
          
  def get(self, key):
      try:
          value = self.config[key]
          return value
      except KeyError:
          log.error("Key does not exist, returning None")
          return
  
  def register_set_function(self, key, function, apply_now=True):
      self.set_functions[key] = function
      if apply_now:
        log.info("Applying %s with %s" % (key, str(self.config[key])))
        self.set_functions[key](key, self.config[key])
      else:
        log.debug("Registering function for %s key..", key)
  
  def apply_all(self):
      log.debug("Running all set functions..")
      for key in self.set_functions.keys():
          self.set_functions[key](key, self.config[key])

def aright(x, w): return x.rjust(w)
def aleft(x, w): return x.ljust(w)
def acenter(x, w): return x.center(w)

def toggle(f1, f2, a1=(), a2=()):
  def true(x): return True
  return lambda k,v: v and true(f1(*a1)) or f2(*a2)

def get_stat(obj, k, v = {}, f = {}):
  if k in v: return v[k]
  if k in f: return f[k]()
  return getattr(obj, k)

def fsize(fsize_b, divide = 1024.0):
    fsize_kb = float (fsize_b / divide)
    if fsize_kb < 1000:
        return ("%3.1fK")%fsize_kb
    fsize_mb = float (fsize_kb / divide)
    if fsize_mb < 1000:
        return ("%3.1fM")%fsize_mb
    fsize_gb = float (fsize_mb / divide)
    return ("%3.1fG")%fsize_gb 

def fpcnt(dec):
    return '%3.3f%%'%(dec * 100)

def fratio(ratio):
    return '%2.2f'%(ratio)

def fbar(progress, width=10):
    progress_chars = int(progress * width)
    return (progress_chars * '#' + (width - progress_chars) * '-')

def fspeed(bps):
  if bps < 0: return 'inf'
  return fsize(bps, 1000)

def fpeers(x, max=999):
  if x > max: return 'inf'
  return str(x)
    
def ftime(seconds):
    if seconds < 60: return '%2ds'%(seconds)
    minutes = int(seconds/60)
    seconds = seconds % 60
    if minutes < 60: return '%2dm %2ds'%(minutes, seconds)
    hours = int(minutes/60)
    minutes = minutes % 60
    if hours < 24: return '%2dh %2dm'%(hours, minutes)
    days = int(hours/24)
    hours = hours % 24
    if days < 7: return '%2dd %2dh'%(days, hours)
    weeks = int(days/7)
    days = days % 7
    if weeks < 10: return '%2dw %2dd'%(weeks, days)
    return 'unknown'

def fstate(x):
  def queue(x):
    if x < 0: return '  '
    if x > 999: return '---'
    return str(x)
  def state(s):
    state = ['queue', 'hash', 'meta', 'down', 'finish', 'seed', 'init']
    return state[s][0]
  def managed(x):
    if x: return 'm'
    return ' '
  def paused(p):
    if p: return 'p'
    return ' '
  q, m, s, p = x
  return "%3s %c%c%c" % (queue(q), managed(m), paused(p), state(s))

def get_default_dir(extra=None):
  path = os.environ.get("HOME")
  if extra:
    path = os.path.join(path, extra)
    try: os.makedirs(path)
    except: pass
  return path
    
def get_config_dir(filename=None):
    path = os.path.join(get_default_dir(), ".tore/")
    
    try: os.mkdir(path)
    except: pass
    
    if filename: path = os.path.join(path, filename)
    return path
