import Pyro.core, Pyro.errors
from socket import gethostname, gethostbyname
from threading import Thread
from common import log, test

def ping(uri):
  try: Pyro.core.getProxyForURI(uri)
  except: return False
  return True

def run(obj, name, client = False, daemon = None):
  Pyro.core.initServer()
  try:
    if client: Pyro.core.initClient()
    daemon = Pyro.core.Daemon()
    uri = daemon.connect(obj, name)
    addr = gethostbyname(gethostname())
    if client: obj.register(addr, obj.get_core())
    obj.start()
    log.info(uri)
    daemon.requestLoop(lambda: obj.looping, 1)
  except: pass
  finally: 
    if obj:
      if not client: obj.publish(None)
      obj.looping = False
      obj.shutdown()
    if daemon: daemon.shutdown()
    

class PyroBase(Thread, Pyro.core.ObjBase):
    def __init__(self):
      Pyro.core.ObjBase.__init__(self)
      Thread.__init__(self)
      self.setDaemon(True)
      self.looping = True
    
    def run(self):
      self.go()
      self.looping = False
    
def PyroClient(Client, server=None):
  class PyroClient(PyroBase, Client):
    def __init__(self, uri):
      PyroBase.__init__(self)
      Client.__init__(self)
      self.uri = uri
    
    def get_core(self):
      return Pyro.core.getProxyForURI(self.uri)

    def register(self, name, core):
      core.join(name, self.getProxy())
      
  if not server: server = gethostname()
  uri = "PYROLOC://"+server+":7766/tord"
  if ping(uri):  
    run(PyroClient(uri), "torc", True)

def PyroServer(Server, name):
  class PyroServer(PyroBase, Server):
    def __init__(self):
      PyroBase.__init__(self)
      Server.__init__(self)

      self.publishMutex = Pyro.util.getLockObject()
      self.clients = []
    
    @test
    def killServer(self):
      self.publish(None)
      self.looping = False
    
    def join(self, name, cb):
      log.debug("PYRO joined: " + name)
      self.clients.append((name, cb))
    
    @test
    def publish(self, msg):
      for name, cb in self.clients:
        try:
          self.publishMutex.acquire()
          try:
            cb._transferThread()
            cb.message(msg)
          finally:
            self.publishMutex.release()
        except Pyro.errors.ConnectionClosedError, x:
          log.debug("PYRO removed: %s %s" % (name, x))
          if (name, cb) in self.clients:
            self.clients.remove((name, cb))
        except Pyro.errors.ProtocolError, x:
          log.error("Pyro.protoco: " + str(x))
        except RuntimeError, x:
          log.error("runtime: " + str(x))
 
  run(PyroServer(), "tord")
