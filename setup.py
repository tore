from setuptools import setup, find_packages, Extension
from setuptools import setup, find_packages, Extension
from distutils import sysconfig
from distutils.core import  Command

#from distutils.core import setup, Extension

import platform
import glob
import os

python_version = platform.python_version()[0:3]


for remove in ['-g', '-Wstrict-prototypes']:
  sysconfig.get_config_vars()["CFLAGS"] = sysconfig.get_config_vars()["CFLAGS"].replace(remove, " ")

# The libtorrent extension
_extra_compile_args = [
    "-Wno-missing-braces",
    "-DHAVE_INCLUDE_LIBTORRENT_ASIO____ASIO_HPP", 
    "-DHAVE_INCLUDE_LIBTORRENT_ASIO_SSL_STREAM_HPP", 
    "-DHAVE_INCLUDE_LIBTORRENT_ASIO_IP_TCP_HPP",
    "-DTORRENT_LINKING_SHARED",
    "-DTORRENT_BUILDING_SHARED",
    "-DNDEBUG",
    #"-g",
    #"-pg",
    "-DBOOST_MULTI_INDEX_DISABLE_SERIALIZATION",
    "-DBOOST_THREAD_USE_LIB",
#    "-DBOOST_NO_EXCEPTIONS",
    "-D_FILE_OFFSET_BITS=64",
    "-DHAVE_PTHREAD",
    "-DTORRENT_USE_OPENSSL",
    "-DHAVE_SSL",
    "-DTORRENT_DISABLE_GEO_IP",
]

_include_dirs = [
    './libtorrent',
    './libtorrent/include',
    './libtorrent/include/libtorrent',
    '/usr/include/python' + python_version,
    '/opt/local/include/'
]
                        
_libraries = [
    'boost_filesystem-mt',
    'boost_date_time-mt',
    'boost_thread-mt',
    'boost_python-mt',
    'boost_iostreams-mt',
    #'boost_system-mt',
    #'GeoIP',
    'z',
    'pthread',
    'ssl'
]
			
_sources = glob.glob("./libtorrent/src/*.cpp") + \
                        glob.glob("./libtorrent/src/kademlia/*.cpp") + \
                        glob.glob("./libtorrent/bindings/python/src/*.cpp")

for source in _sources:
    if "file_win.cpp" in source: _sources.remove(source)
    if "mapped_storage.cpp" in source: _sources.remove(source)

libtorrent = Extension(
    'libtorrent',
    include_dirs = _include_dirs,
    library_dirs = ["/opt/local/lib/"],
    libraries = _libraries,
    extra_compile_args = _extra_compile_args,
    sources = _sources,
    define_macros = []
)

pytx = Extension(
    'pytx',
    sources = glob.glob("./pytx/*.c"),
    libraries = ['ncurses', 'panel'],
    extra_compile_args = ['-g', '-pg']
)

class SelfUpdate( Command ):
  descritpion = 'Perform a "svn up"'
  user_options = [ ]
  
  def initialize_options( self ): pass
  
  def finalize_options( self ): pass

  def run( self ):
    os.chdir('./libtorrent')
    os.system('svn up')
    os.system('svn diff')
    os.system('git commit -a -m AUTO_LT_SYNC')

setup(
    cmdclass = {'Update': SelfUpdate},
    name = "tore",
    fullname = "Tore: client/daemon bittorrent software shipped with a NCurses client",
    version = "0.3",
    author = "Josh Davis",
    author_email = "sivad77@gmail.com",
    include_package_data = True,
    description = "Curses bittorrent client/daemon",
    ext_modules = [pytx, libtorrent],
    entry_points = """
        [console_scripts]
            tord = tord.tord:main
            torc = torc.torc:main
    """
)
