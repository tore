#include <ncurses.h>
#include <assert.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <assert.h>
#include <sys/ioctl.h>
#include "txops.h"
#include "txlist.h"

char *tx_text_blank(char *t, int w) {
  char *b;
  
  b = malloc(sizeof(char) * w);
  assert(b);

  memset(b, ' ', w);
  if (t) memcpy(b, t, MIN(strlen(t), w));
  b[w-1] = 0;
  return b;
}

void tx_box(WINDOW *win) {
  int winwidth;
  int winheight;

  scrollok(win, 0);
  
  getmaxyx(win, winheight, winwidth);
  winheight--;
  winwidth--;
  
  mvwaddch(win, 0, 0, ACS_ULCORNER);
  mvwaddch(win, winheight, 0, ACS_LLCORNER);
  mvwaddch(win, 0, winwidth, ACS_URCORNER);
  mvwaddch(win, winheight, winwidth, ACS_LRCORNER);
  
  mvwhline(win, 0, 1, ACS_HLINE, winwidth - 1);
  mvwhline(win, winheight, 1, ACS_HLINE, winwidth - 1);
  mvwvline(win, 1, 0, ACS_VLINE, winheight - 1);
  mvwvline(win, 1, winwidth, ACS_VLINE, winheight - 1);
  doupdate();
}

void tx_ungetc(int c) {
  ungetch(c);
}

void tx_resize(void) {
  struct winsize ws;
  int r;
  r = ioctl(0, TIOCGWINSZ, &ws);
  assert(r != -1);

  r = resizeterm(ws.ws_row, ws.ws_col);
  assert(r != -1);
}

void signals(int sig) {
  switch (sig) {
    case SIGWINCH: tx_resize(); break;
  }
}

void tx_init(void) {
  initscr();
  cbreak();
  noecho();
  nonl();
  keypad(stdscr, 1);
  curs_set(0);
  clear();
  signal(SIGWINCH, signals);
  //signal(SIGINT, sighandler);
  
  /* set to globals to NULL */
  tx_obj_root = NULL;
  tx_obj_focus = NULL;
}

void tx_free(void) {
  //clear();
  //refresh();
  endwin();
}

void tx_stdwinset(WINDOW * win) {
  meta(win, TRUE);
  keypad(win, TRUE);
  notimeout(win, 0);
  scrollok(win, 1);
}

void tx_refresh_screen(void) {
  endwin();
  doupdate();
  curs_set(0);
}
