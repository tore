#ifndef _TXOBJ_H_
#define _TXOBJ_H_
#include <ncurses.h>
#include <panel.h>
#include "list.h"

#define TX_EXT(type, var) type *var = (type *)obj->extend
#define MIN(a,b) (((a) < (b) ? (a) : (b)))
#define MAX(a,b) (((a) < (b) ? (a) : (b)))

typedef struct _TXObj TXObj;
  
typedef void (*obj_fn) (TXObj *);
typedef int  (*getc_fn)(TXObj *);
typedef int  (*draw_fn)(TXObj *);
typedef int  (*size_fn)(TXObj *, int, int);
typedef void (*oper_fn)(TXObj *, int);

struct _TXObj {
  struct list_head node;
  void *extend;
  
  char *title;
  int x, y, h, w;
  
  int flags;
  int type;

  WINDOW *bwin;
  PANEL *bpanel;
  WINDOW *win;
  PANEL *panel;

  obj_fn  init_cb;
  obj_fn  rsze_cb;
  obj_fn  idle_cb;
  draw_fn  draw_cb;
  getc_fn getc_cb;
  size_fn size_cb;
  oper_fn oper_cb;
  
  /* passed to the above functions */
  void *data;
  
  /* spare variable, useful for bindings */
  void *bind;

};

extern TXObj *tx_obj_root;
extern TXObj *tx_obj_focus;

enum {TX_LIST = 1, TX_BOX = 2};
/* upto 10 */
/* XXX: BORDER MUST BE 1 << 0 because it uses the value 1 as an offset when drawing */
enum {TX_OBJ_BORDER = 1<<0, TX_OBJ_ROOT = 1<<1, TX_OBJ_FOCUS = 1<<2, TX_OBJ_TITLE = 1<<3};

int  tx_obj_init          (TXObj *obj, int type, int flags);
void tx_obj_border        (TXObj *obj);
void tx_obj_free          (TXObj *obj);
void tx_obj_get_type      (TXObj *obj);
void tx_obj_reset         (TXObj *obj);

void tx_obj_title_set     (TXObj *obj, char *title);
void tx_obj_size_set      (TXObj *obj, int x, int y, int w, int h);
void tx_obj_map           (TXObj *obj, void (*fn)(TXObj *));
void tx_obj_flag_set      (TXObj *obj, int flag, int set);
int  tx_obj_flag_get      (TXObj *obj, int flag);
int  tx_obj_flag_toggle   (TXObj *obj, int flag);


/* object functions */
void tx_obj_draw          (TXObj *obj);
void tx_obj_resize        (TXObj *obj);
void tx_obj_idle          (TXObj *obj);
void tx_obj_operate       (TXObj *obj);

/* set object cb functions */
void tx_obj_size_cb_set   (TXObj *obj, size_fn);
void tx_obj_rsze_cb_set   (TXObj *obj, obj_fn);
void tx_obj_idle_cb_set   (TXObj *obj, obj_fn);
void tx_obj_getc_cb_set   (TXObj *obj, getc_fn);

#endif
