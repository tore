#ifndef _TXBOX_H_
#define _TXBOX_H_

#include <ncurses.h>
#include <panel.h>
#include "txobj.h"
#include "list.h"

typedef struct _TXBox TXBox;
  
struct _TXBox {
  struct list_head node;
  TXObj *obj;

  int count;
};

/* upto 20 */
enum {TX_BOX_VERT = 1<<15, TX_BOX_HORZ = 1<<16}; 

void tx_box_init(TXObj *obj);
void tx_box_add(TXObj *box, TXObj *obj);
void tx_box_adjust(TXObj *obj);
int tx_box_draw(TXObj *obj);

#endif
