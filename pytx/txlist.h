#ifndef _TXLIST_H_
#define _TXLIST_H_

#include <ncurses.h>
#include <panel.h>
#include "txobj.h"
#include "list.h"

typedef struct _TX_List TX_List;
typedef struct _TXColumns TXColumns;
  
struct Row {
  struct list_head node;
  char *text;
  void *data;
  int key;
};

typedef int    (*cmpr_fn)(TXObj *,
                          const struct list_head *,
                          const struct list_head *);
typedef void   (*bind_fn)(TXObj *, int ch, int key);
typedef char  *(*text_fn)(TXObj *, char *, void *);
typedef void  *(*data_fn)(TXObj *, void *, void *);
typedef char **(*cols_fn)(TXObj *, char *, void *);

struct _TX_List {
  struct Row **map;       /* integer mapping to rows */
  struct list_head rows;  /* sorted rows */
  struct list_head *top;
  struct list_head *sel;
  
  int size;               /* max amount of rows before new allocation */
  int count;              /* amount of rows */
  int asc;                /* ascending sort */

  struct _TXColumns {
    int    ncols;
    int    sort;               
    int   *widths;
    int   *offsets;
    char **titles;
  } cols;

  obj_fn  draw_cb;
  cmpr_fn cmpr_cb;
  bind_fn bind_cb;
  text_fn text_cb;
  data_fn data_cb;
  cols_fn cols_cb;
};

/* upto 15 */
enum {
  TX_LIST_SCROLL = 1<<10,
  TX_LIST_SEPARATOR = 1<<11,
  TX_LIST_TITLE = 1<<12,
  TX_LIST_HLIGHT = 1<<13
};

void  tx_list_new               (TXObj *obj);
void  tx_list_init              (TXObj *obj);
void  tx_list_sort              (TXObj *obj);
void  tx_list_sort_asc_set      (TXObj *obj, int asc);
int   tx_list_sort_asc_get      (TXObj *obj);
void  tx_list_clear             (TXObj *obj);
void  tx_list_sync              (TXObj *obj);
void  tx_list_row_remove        (TXObj *obj, int k);
void  tx_list_scroll            (TXObj *obj, int scroll);
void  tx_list_row_sync          (TXObj *obj, int k, void *d);
int   tx_list_selected_get      (TXObj *obj);
void  tx_list_selected_set      (TXObj *obj, int k); /* NI */


void  tx_list_cols_cb_set       (TXObj *obj, cols_fn);
void  tx_list_text_cb_set       (TXObj *obj, text_fn);
void  tx_list_data_cb_set       (TXObj *obj, data_fn);
void  tx_list_cmpr_cb_set       (TXObj *obj, cmpr_fn);
void  tx_list_bind_cb_set       (TXObj *obj, bind_fn);


int  *tx_list_columns_width_get (TXObj *obj);
void  tx_list_columns_width_set (TXObj *obj, int ncols, int *widths);
void  tx_list_columns_title_set (TXObj *obj, int ncols, char **titles);
void  tx_list_column_sort_set (TXObj *obj, int i);
int   tx_list_column_sort_get (TXObj *obj); /* NI */
#endif
