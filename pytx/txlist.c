/*
 * listbox.c - scrollable listbox
 *
 * Written by Joshua Davis
 * Copyright (c) Josh Davis 2008
 */

#include <ncurses.h>
#include <math.h>
#include <panel.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "sort.h"
#include "txlist.h"
#include "txops.h"
#include "txbox.h"

/* default cb for list of strings */
static char *text_cb(TXObj *obj, char *o, void *d) {
  return d;
}

static void *data_cb(TXObj *obj, void *o, void *d) {
  if (o) free(o);
  return tx_text_blank(d, obj->w);
}

static int cmpr_cb(TXObj *obj,
                   const struct list_head *a,
                   const struct list_head *b) {
  TX_EXT(TX_List, list);
  struct Row *ra = list_entry(a, struct Row, node);
  struct Row *rb = list_entry(b, struct Row, node);

  return list->asc ? strcmp(ra->data, rb->data) : strcmp(rb->data, ra->data);
}

/* functions to set cbs, ie for more complicated 'data' and routines to process
 * the 'data' into 'text' */
void tx_list_text_cb_set(TXObj *obj, text_fn text_cb) {
  TX_EXT(TX_List, list);
  list->text_cb = text_cb;
}

void tx_list_data_cb_set(TXObj *obj, data_fn data_cb) {
  TX_EXT(TX_List, list);
  list->data_cb = data_cb;
}

/* function to set cmpr_cb to work with 'data' */
void tx_list_cmpr_cb_set(TXObj *obj, cmpr_fn cmpr_cb) {
  TX_EXT(TX_List, list);
  list->cmpr_cb = cmpr_cb;
}

/* function to set bind_cb to hand keyevents */
void tx_list_bind_cb_set(TXObj *obj, bind_fn bind_cb) {
  TX_EXT(TX_List, list);
  list->bind_cb = bind_cb;
}



void tx_list_map_resize(TXObj *obj, int n) {
  int i;
  TX_EXT(TX_List, list);

  list->map = realloc(list->map, sizeof(struct Row*) * (n + 1));
  assert(list->map);

  /* if we are adding an item with a key greater then the current size we have
   * to set all the 'in between' items to NULL */
  for (i = list->size; i < n + 1; ++i)
    list->map[i] = NULL;
  list->size = n + 1;
}

void tx_list_row_init(TXObj *obj, int n) {
  TX_EXT(TX_List, list);
  
  list->map[n] = calloc(1, sizeof(struct Row));
  assert(list->map[n]);
  list->count++;
  list_add_tail(&list->map[n]->node, &list->rows);
  if (!list->sel) list->sel = list->top = list->rows.next;
}

void tx_list_row_sync(TXObj *obj, int k, void *d) {
  TX_EXT(TX_List, list);
  struct Row *row;

  if (k > list->size - 1) tx_list_map_resize(obj, k);
  if (!list->map[k]) tx_list_row_init(obj, k);
  row = list->map[k];
  row->data = list->data_cb(obj, row->data, d);
  row->text = list->text_cb(obj, row->text, row->data);
  row->key = k;
}

void tx_list_sync(TXObj *obj) {
  TX_EXT(TX_List, list);
  int i;
  
  for (i = 0; i < list->count; ++i)
    if (list->map[i])
      list->map[i]->text = list->text_cb(obj,
                                         list->map[i]->text,
                                         list->map[i]->data);
}

int tx_list_rows_draw(TXObj *obj,
                       struct list_head *top,
                       struct list_head *bot,
                       int i,
                       int b) {
  TX_EXT(TX_List, list);
  struct Row *row;
  struct list_head *item;
  
  wbkgdset(obj->win, '\0' | A_NORMAL);
  for (item = top; i < b && item != bot; item = item->next, ++i) {
    row = list_entry(item, struct Row, node);
    mvwaddnstr(obj->win, i, 0, row->text, obj->w);
    if ((obj->flags & TX_LIST_SCROLL) && item == list->sel) {
      wbkgdset(obj->win, '\0' | A_STANDOUT);
      mvwaddnstr(obj->win, i, 0, row->text, obj->w);
      wbkgdset(obj->win, '\0' | A_NORMAL);
    }
  }
  /* so we know if we fully drew the list */
  return i;
}

int tx_list_draw(TXObj *obj) {
  TX_EXT(TX_List, list);
  int ret;

  if (!list->top) return -1;
  ret = tx_list_rows_draw(obj, list->top, &list->rows, 0, MIN(obj->h, list->count));
  if (list->draw_cb) list->draw_cb(obj);

  return ret;
}

void tx_list_clear(TXObj *obj) {
  TX_EXT(TX_List, list);
  int i;
  
  /* FIXME: proper free */
  for (i = 0; i < list->count; ++i)  
    list->map[i] = NULL;

  /* reset map */
  list->count = 0;
  list->size = 0;
  
  /* reset list */
  list_init(&list->rows);
  list->sel = list->top = NULL;
  
  werase(obj->win);
}

void tx_draw_blank_line(TXObj *obj, int i) {
  char *text;
  text = tx_text_blank(NULL, obj->w);
  mvwaddnstr(obj->win, i, 0, text, obj->w);
  free(text);
}

void tx_list_row_remove(TXObj *obj, int k) {
  TX_EXT(TX_List, list);
  struct Row *row = list->map[k];
  int i;

  if (!row) return; 
  list->sel = row->node.next;
  if (row == list->top)
    list->top = list->sel;

  list_del(&row->node);
  
  /* FIXME: proper free */
  list->map[k] = NULL;
  list->count--;

  i = tx_list_draw(obj);

  if (i < obj->h)
    tx_draw_blank_line(obj, i);
}

/* we 'walk' over the items in the list either forwards or backwards and set
 * selected */ 
void tx_list_row_walk(TXObj *obj, int s) {
  TX_EXT(TX_List, list);
  struct list_head *up, *down;
  int i, u;

  up = down = list->sel;
  u = s < 0 ? 1 : 0;
  s = abs(s);
  
  /* iterate over list until a) we hit the top b) we exceep scroll */
  for (i = 0; i < s; i++) {
    /* ensure scrolls greater then 1 do not loop around */
    if (!u && down->next == &list->rows) {
      list->sel =  i ? list->rows.prev : list->rows.next;
      return;
    }
    if (u && up->prev == &list->rows) {
      list->sel =  i ? list->rows.next : list->rows.prev;
      return;
    }
    up = up->prev;
    down = down->next;
  }
  /* we did not hit the top or bottom */
  list->sel = u ? up : down;
}


/* this is tricky, perhaps dirty. 1) try to make the selected item occur in the
 * middle of the list. this is possible iff there are enough items above and
 * below the selected item. 2) place selected item closer to the top or bottom
 * if 1) is not possible */
void tx_list_row_set_top(TXObj *obj) {
  TX_EXT(TX_List, list);
  int i,u,d,s,o;
  struct list_head *up, *down;
  
  if (list->count < obj->h) {
    list->top = list->rows.next;
    return;
  }
  
  s = obj->h/2;
  /* if s is not in the middle, we must set an offset of 1. this fixes the
   * selected item be the very bottom item in the list */
  o = (obj->h + 1) % 2;
  up = list->sel;
  down = list->sel->next;
  
  /* determin if we can hit the bottom */
  for (d = 0; d < s; d++) {
    if (down == &list->rows) break;
    down = down->next;
  }
  
  /* can we hit the top */
  for (u = 0; u < s; u++) {
    if (up == list->rows.next) break;
    up = up->prev;
  }
  
  /* displace top if at the bottom of the list */
  if (d < s) {
    for (i = u - d - o; i > 0;  i--) {
      if (up == &list->rows) break;
      up = up->prev;
    }
  }

  list->top = up;
}

/* using wscrl is about twice as fast, but more complicated so scrapped as 1%
 * to 2x is nothing*/
void tx_list_scroll(TXObj *obj, int scroll) {
  tx_list_row_walk(obj, scroll);
  tx_list_row_set_top(obj);
  tx_obj_draw(obj);
  //wscrl(obj->win, scroll);
  //update_panels();
  //doupdate();
}

int tx_list_selected_get(TXObj *obj) {
  TX_EXT(TX_List, list);
  struct Row *row;
  if (list->sel) {
    row = list_entry(list->sel, struct Row, node);
    return row->key;
  }
  /* no item is selected, ie list is empty */
  return -1;
}

void tx_list_selected_set(TXObj *obj, int key) {
  TX_EXT(TX_List, list);
  struct Row *row;
  struct list_head *item;
  
  list_for_each(item, list->sel) {
    row = list_entry(item, struct Row, node);
    if (row->key == key)
      break;
  }

  list->sel = item;
  tx_list_row_set_top(obj);
}

void tx_list_operate(TXObj *obj, int key) {
  TX_EXT(TX_List, list);
  switch(key) {
    case KEY_UP:
      if (obj->flags & TX_LIST_SCROLL) tx_list_scroll(obj, -1);
    break;
    case KEY_DOWN:
      if (obj->flags & TX_LIST_SCROLL) tx_list_scroll(obj, +1);
    break;
    case KEY_PPAGE:
      if (obj->flags & TX_LIST_SCROLL) tx_list_scroll(obj, -obj->h/2);
    break;
    case KEY_NPAGE:
      if (obj->flags & TX_LIST_SCROLL) tx_list_scroll(obj, obj->h/2);
    break;
    default:        if (list->bind_cb) list->bind_cb(obj, key, tx_list_selected_get(obj)); break;

    /* FIXME: testing, delete at some stage */
    //case 'D':       tx_list_row_remove(obj, tx_list_selected_get(obj)); tx_obj_draw(obj); break;
    case 'C':       tx_list_clear(obj); tx_obj_draw(obj); break;
    case 'B':       tx_obj_flag_toggle(obj, TX_OBJ_BORDER); tx_obj_reset(obj); tx_obj_draw(obj); break;
    case 'S':       tx_obj_flag_toggle(obj, TX_LIST_SEPARATOR); tx_obj_reset(obj); tx_obj_draw(obj); break;
    case 'T':       tx_obj_flag_toggle(obj, TX_LIST_TITLE); tx_obj_reset(obj); tx_list_sync(obj); tx_obj_draw(obj); break;
  }
}

void tx_list_sort_asc_set(TXObj *obj, int asc) {
  TX_EXT(TX_List, list);
  list->asc = asc;
}

int tx_list_sort_asc_get(TXObj *obj) {
  TX_EXT(TX_List, list);
  return list->asc;
}

void tx_list_sort(TXObj *obj) {
  TX_EXT(TX_List, list);
  sort(&list->rows, obj, list->cmpr_cb);
  tx_list_row_set_top(obj);
}

/* ---------------- column stuff ----------------------- */
/* FIXME: abstract out */
/* draw in lines */


void tx_list_columns_line_draw(TXObj *obj) {
  TX_EXT(TX_List, list);
  int i, border = obj->flags & TX_OBJ_BORDER;
  
  if (!(obj->flags & TX_LIST_SEPARATOR)) return;
  wattrset(obj->win, A_NORMAL);
  for (i = 1; i < list->cols.ncols; ++i) {
    mvwvline(obj->win,
             0,
             list->cols.offsets[i] - 1,
             ACS_VLINE,
             MIN(obj->h, list->count));
    if ((obj->flags & TX_LIST_TITLE) && list->cols_cb) {
      mvwvline(obj->bwin, 0 + border, list->cols.offsets[i] - 1 + border, ACS_VLINE, 1);
      mvwaddch(obj->bwin, 1 + border, list->cols.offsets[i] - 1 + border, ACS_PLUS);
    }
  }
}

char *tx_list_columns_format(TXColumns *cols, char **c, int w) {
  int i;
  char *text = tx_text_blank(NULL, w);

  for(i = 0; i < cols->ncols; ++i)
    memcpy(text + cols->offsets[i], c[i], MIN(cols->widths[i], strlen(c[i])));

  return text;
}

/* NOTE: perhaps we could save each col of each row, then hightlight the whole
 * column eg abscgract this out to take a win and col structures */
void tx_list_column_title_draw(TXObj *obj) {
  TX_EXT(TX_List, list);
  char *titles;
  int hl = list->cols.sort;
  int border = obj->flags & TX_OBJ_BORDER;

  if (!(obj->flags & TX_LIST_TITLE && list->cols.ncols)) return;

  titles = tx_list_columns_format(&list->cols, list->cols.titles, obj->w);
  mvwaddnstr(obj->bwin, border, border, titles, obj->w);
  free(titles);

  /* highlight selected */
  if (!(obj->flags & TX_LIST_HLIGHT)) return;

  wbkgdset(obj->bwin, '\0' | A_STANDOUT);
  mvwaddnstr(obj->bwin,
             border,
             list->cols.offsets[hl]+border,
             list->cols.titles[hl],
             list->cols.widths[hl]);

  wbkgdset(obj->bwin, '\0' | A_NORMAL);
}

void tx_list_column_sort_set(TXObj *obj, int col) {
  TX_EXT(TX_List, list);
  list->cols.sort = col;
  tx_list_column_title_draw(obj);
}

int tx_list_column_sort_get(TXObj *obj) {
  TX_EXT(TX_List, list);
  return list->cols.sort;
}

/* wrapper to convert list of strings into a string */
static char *text_cols_cb(TXObj *obj, char *o, void *d) {
  TX_EXT(TX_List, list);
  char *text;
  char **cols = list->cols_cb(obj, o, d);
  text =  tx_list_columns_format(&list->cols, cols, obj->w);
  free(cols);
  return text;
}

void tx_list_cols_cb_set(TXObj *obj, cols_fn cols_cb) {
  TX_EXT(TX_List, list);

  list->text_cb = text_cols_cb;
  list->cols_cb = cols_cb;
  list->draw_cb = tx_list_columns_line_draw;
}

void tx_list_columns_width_adjust(TXColumns *cols, int w, int f) {
  int i, o;
  
  for (i = o = 0; i < cols->ncols-1; ++i) {
    if (f) cols->widths[i] = f;
    cols->offsets[i] = o;
    o += cols->widths[i] + 1;
  }
  
  cols->offsets[i] = o;
  cols->widths[i] = w-o-1;
  assert(cols->widths[i] > 0);
}

/* FIXME add assumption that data is an array of strings, hence set col_cb */
void tx_list_columns_width_set(TXObj *obj, int ncols, int *widths) {
  TX_EXT(TX_List, list);
  int i, t;

  list->cols.ncols = ncols;
  list->cols.widths = realloc(list->cols.widths, sizeof(int) * ncols);
  list->cols.offsets = realloc(list->cols.offsets, sizeof(int) * ncols);
  assert(list->cols.offsets);
  assert(list->cols.widths);

  for (t = i = 0; i < ncols; ++i) {
    t += widths[i];
    list->cols.widths[i] = widths[i];
  }
  /* if the sum of the widths is greater then the width of the box, give each
   * col an equal width */
  tx_list_columns_width_adjust(&list->cols,
                               obj->w,
                               t > obj->w ? obj->w/ncols : 0);
}

/* FIXME: should i copy this out */
int *tx_list_columns_width_get(TXObj *obj) {
  TX_EXT(TX_List, list);

  return list->cols.widths;
}

void tx_list_columns_title_set(TXObj *obj, int ncols, char **titles) {
  TX_EXT(TX_List, list);
  int i;

  list->cols.titles = realloc(list->cols.titles, sizeof(char *) * ncols);
  assert(list->cols.titles);
  
  /* FIXME: what if new columns has less columns etc */
  /* add the titles */
  for (i = 0; i < ncols; ++i)
    //if (list->cols.titles[i]) free(list->cols.titles[i]);
    list->cols.titles[i] = titles[i];

  /* draw the titles */
  tx_list_column_title_draw(obj);
}

void tx_list_init(TXObj *obj) {
  int border = obj->flags & TX_OBJ_BORDER;
  
  if (obj->flags & TX_LIST_TITLE) {
    //fprintf(stderr,"%d %d %d %d\n", obj->x, obj->y, obj->w, obj->h);
    tx_obj_size_set(obj, obj->x, obj->y+2, obj->w, obj->h-2);
    mvwhline(obj->bwin, 1+border, 0+border, ACS_HLINE, obj->w);
    tx_list_column_title_draw(obj);
  }
  
  if (obj->flags & TX_LIST_SCROLL) 
    scrollok(obj->win, 1);
  
  /* resize existing window */ 
  if (obj->win) {
    wresize(obj->win, obj->h, obj->w);
    mvwin(obj->win, obj->y, obj->x);
    tx_list_sync(obj);
    return;
  }
  
  /* make new window */
  obj->win = newwin(obj->h, obj->w, obj->y, obj->x);
  assert(obj->win);
  obj->panel = new_panel(obj->win);
}

void tx_list_new(TXObj *obj) {
  obj->extend = calloc(1, sizeof(TX_List));
  assert(obj->extend);
  TX_EXT(TX_List, list);

  obj->draw_cb = tx_list_draw;
  obj->oper_cb = tx_list_operate;
  obj->init_cb = tx_list_init;
  
  /* we add strings by default */
  list->cmpr_cb = cmpr_cb;
  list->data_cb = data_cb;
  list->text_cb = text_cb;
  
  list_init(&list->rows);
}
