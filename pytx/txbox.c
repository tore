/*
 * listbox.c - box, a container
 *
 * Written by Joshua Davis
 * Copyright (c) Josh Davis 2008
 */

#include <ncurses.h>
#include <panel.h>
#include <string.h>
#include <stdlib.h>
#include "txbox.h"
#include "txops.h"
#include "list.h"

void tx_box_adjust(TXObj *obj) {
  int dyn, sta, size, start, left;
  TXObj *entry;
  TX_EXT(TXBox, box);
  dyn = (obj->flags & TX_BOX_VERT) ? obj->h : obj->w;
  sta = (obj->flags & TX_BOX_VERT) ? obj->w : obj->h;
  start = (obj->flags & TX_BOX_VERT) ? obj->y : obj->x;
  left = box->count;
  
  list_for_each_entry(entry, &box->node, node) {
    size = (entry->size_cb) ? entry->size_cb(entry, sta, dyn) : dyn / left;
    if (obj->flags & TX_BOX_VERT) tx_obj_size_set(entry, obj->x, start, obj->w, size);
    if (obj->flags & TX_BOX_HORZ) tx_obj_size_set(entry, start, obj->y, size, obj->h);
    //tx_obj_resize(entry);
    dyn -= size;
    start += size;
    left--;
  }
}

void tx_box_add(TXObj *obj, TXObj *add) {
  TX_EXT(TXBox, box);
  
  list_add_tail(&add->node, &box->node);
  box->count++;
}

int tx_box_draw(TXObj *obj) {
  TX_EXT(TXBox, box);
  TXObj *entry;
   
  list_for_each_entry(entry, &box->node, node) {
    entry->draw_cb(entry);
  }

  return 0;
}

void tx_box_rsze(TXObj *obj) {
  tx_box_adjust(obj);
}

void tx_box_init(TXObj *obj) {
  obj->extend = calloc(1, sizeof(TXBox));
  TX_EXT(TXBox, box);
  obj->draw_cb = tx_box_draw;
  obj->rsze_cb = tx_box_rsze;
  list_init(&box->node);

}
