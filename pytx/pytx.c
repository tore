#include <Python.h>

#include "txlist.h"
#include "txbox.h"
#include "txops.h"
#include "txobj.h"

/* -------------------------------------------------*/
typedef struct {
  PyObject *text_cb;
  PyObject *idle_cb;
  PyObject *bind_cb;
  PyObject *cmpr_cb;
  PyObject *size_cb;
  PyObject *rsze_cb;
} PyTXList_Funcs;

typedef struct {
  PyObject_HEAD
  TXObj tx_obj;
  void *data;
} PyTX_Obj;

/* object functions */

static PyObject *PyTXObj_draw(PyTX_Obj *self) {
  tx_obj_draw(&(self->tx_obj));
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTXObj_flag_set(PyTX_Obj *self, PyObject *args) {
  int flag, set;

  PyArg_ParseTuple(args, "ii", &flag, &set);

  tx_obj_flag_set(&(self->tx_obj), flag, set);
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTXObj_flag_toggle(PyTX_Obj *self, PyObject *args) {
  PyObject *result;
  int flag;

  PyArg_ParseTuple(args, "i", &flag);

  flag = tx_obj_flag_toggle(&(self->tx_obj), flag);
  result = PyInt_FromLong(flag);

  Py_INCREF(result);
	return result;
}

static PyObject *PyTXObj_flag_get(PyTX_Obj *self, PyObject *args) {
  PyObject *result;
  int flag;
  
  PyArg_ParseTuple(args, "i", &flag);

  flag = tx_obj_flag_get(&(self->tx_obj), flag);
  result = PyInt_FromLong(flag);

  Py_INCREF(result);
	return result;
}

static PyObject *PyTXObj_reset(PyTX_Obj *self) {
  tx_obj_reset(&(self->tx_obj));
  Py_INCREF(Py_None);
  return Py_None;
}

/* list functions */
static void PyTXList_free(PyTX_Obj* self) {
  self->ob_type->tp_free((PyObject*)self);
}

static void *data_cb(TXObj *obj, void *o, void *d) {
  if (d&&o) {
    Py_DECREF((PyObject*)o);
  }
  Py_INCREF((PyObject*)d);
  return d;
}

char *text_cb(TXObj *obj, char *o, void *d) {
  PyObject *result, *params;
  PyTXList_Funcs *funcs = (PyTXList_Funcs*)obj->data;
  char *text;

  if (d) free (o);

  params = Py_BuildValue("(OO)", obj->bind, d);
  result = PyEval_CallObject(funcs->text_cb, params);
  text = PyString_AsString(result);
  
  Py_DECREF(result);
  Py_DECREF(params);
  return strdup(text);
}

int cmpr_cb(TXObj *obj, const struct list_head *a, const struct list_head *b) {
  TX_EXT(TX_List, list);
  PyObject *o1, *o2;

  struct Row *ra = list_entry(a, struct Row, node);
  struct Row *rb = list_entry(b, struct Row, node);

  o1 = PyList_GetItem(ra->data, list->cols.sort); 
  o2 = PyList_GetItem(rb->data, list->cols.sort);
  
  return list->asc ? PyObject_Compare(o1, o2) : PyObject_Compare(o2, o1);
}

void idle_cb(TXObj *obj) {
  PyObject *params, *result;
  PyTXList_Funcs *funcs = (PyTXList_Funcs*)obj->data;

  params = Py_BuildValue("(O)", obj->bind);
  result = PyEval_CallObject(funcs->idle_cb, params);
  
  Py_DECREF(params);
  Py_DECREF(result);
}

void bind_cb(TXObj *obj, int ch, int key) {
  PyObject *params, *result;
  PyTXList_Funcs *funcs = (PyTXList_Funcs*)obj->data;

  params = Py_BuildValue("(Oii)", obj->bind, ch, key);
  result = PyEval_CallObject(funcs->bind_cb, params);
  
  Py_DECREF(result);
  Py_DECREF(params);
}

void rsze_cb(TXObj *obj) {
  PyObject *params, *result;
  
  PyTXList_Funcs *funcs = (PyTXList_Funcs*)obj->data;

  params = Py_BuildValue("(O)", obj->bind);
  result = PyEval_CallObject(funcs->rsze_cb, params);
	
  Py_DECREF(result);
  Py_DECREF(params);
}

int size_cb(TXObj *obj, int s, int d) {
  int size;

  PyObject *result, *params;
  PyTXList_Funcs *funcs = (PyTXList_Funcs*)obj->data;


  params = Py_BuildValue("(Oii)", obj->bind, s, d);
  result = PyEval_CallObject(funcs->size_cb, params);
  size = (int)PyInt_AsLong(result);
	
  Py_DECREF(result);
  Py_DECREF(params);

	return size;
}

int getc_cb(TXObj *obj) {
  int key;
  
  Py_BEGIN_ALLOW_THREADS
  key = getch();
  Py_END_ALLOW_THREADS
  
  return key;
}

static int PyTXList_init(PyTX_Obj *self, PyObject *args, PyObject *kwds) {
  int flags;
  PyObject *cb;
  PyTXList_Funcs *funcs;
  PyArg_ParseTuple(args, "|i", &flags);
  tx_obj_init(&(self->tx_obj), TX_LIST, PyTuple_Size(args) ? flags : 0);
  
  tx_obj_getc_cb_set(&(self->tx_obj), getc_cb);

  self->tx_obj.bind = self;
  self->tx_obj.data = calloc(1, sizeof(PyTXList_Funcs));
  assert(self->tx_obj.data);
  funcs = self->tx_obj.data;

  cb = PyDict_GetItemString(self->ob_type->tp_dict, "text_cb");
  if (cb && PyFunction_Check(cb)) {
    tx_list_data_cb_set(&(self->tx_obj), data_cb);
    funcs->text_cb = cb;
  }
  
  cb = PyDict_GetItemString(self->ob_type->tp_dict, "idle_cb");
  if (cb && PyFunction_Check(cb)) {
    tx_obj_idle_cb_set(&(self->tx_obj), idle_cb);
    funcs->idle_cb = cb;
  }
  
  cb = PyDict_GetItemString(self->ob_type->tp_dict, "bind_cb");
  if (cb && PyFunction_Check(cb)) {
    tx_list_bind_cb_set(&(self->tx_obj), bind_cb);
    funcs->bind_cb = cb;
  }
  
  tx_list_cmpr_cb_set(&(self->tx_obj), cmpr_cb);
  
  /* FIXME: restore previous cmpr_cb for custom compare */
  cb = PyDict_GetItemString(self->ob_type->tp_dict, "cmpr_cb");
  if (cb && PyFunction_Check(cb)) {
    tx_list_cmpr_cb_set(&(self->tx_obj), cmpr_cb);
    funcs->cmpr_cb = cb;
  }
  
  cb = PyDict_GetItemString(self->ob_type->tp_dict, "size_cb");
  if (cb && PyFunction_Check(cb)) {
    tx_obj_size_cb_set(&(self->tx_obj), size_cb);
    funcs->size_cb = cb;
  }
  
  cb = PyDict_GetItemString(self->ob_type->tp_dict, "rsze_cb");
  if (cb && PyFunction_Check(cb)) {
    tx_obj_rsze_cb_set(&(self->tx_obj), rsze_cb);
    funcs->rsze_cb = cb;
  }
  
  return 0;
}

char **cols_cb(TXObj *obj, char *o, void *d) {
  PyObject *result, *params, *item;
  int ncols, i;
  char **cols;

  PyTXList_Funcs *funcs = (PyTXList_Funcs*)obj->data;
  
  if (o&&d) free (o);

  params = Py_BuildValue("(OO)", obj->bind, d);
  result = PyEval_CallObject(funcs->text_cb, params);

  ncols =  PyList_Size(result); 
  cols = malloc(sizeof(char *) * ncols);
  assert(cols);

  for (i=0; i < ncols; ++i) {
    item = PyList_GetItem(result, i);
    cols[i] = PyString_AsString(item);
  }

  Py_DECREF(params);
  Py_DECREF(result);
  return cols;
}

static PyObject *PyTXList_columns_width_set(PyTX_Obj *self, PyObject *args) {
  PyObject *py_widths;
  int i, ncols, *widths;
  
  PyArg_ParseTuple(args, "O", &py_widths);

  ncols = PyList_Size(py_widths);
  widths = malloc(sizeof(int) * ncols);
  assert(widths);

  for (i = 0; i < ncols; ++i)
    widths[i] = PyInt_AsLong(PyList_GetItem(py_widths, i));
  
  tx_list_columns_width_set(&(self->tx_obj), ncols, widths);
  free(widths);
  
  widths = tx_list_columns_width_get(&(self->tx_obj));
  for (i = 0; i < ncols; ++i) 
    PyList_SetItem(py_widths, i, PyInt_FromLong(widths[i]));
  
  /* set cols_cb upon setting widths */
  tx_list_cols_cb_set(&(self->tx_obj), cols_cb);
  
  /* give object access to list array */
  PyDict_SetItemString(self->ob_type->tp_dict, "widths", py_widths);

  Py_INCREF(py_widths);
  Py_INCREF(Py_None);
	return Py_None;
  
}

static PyObject *PyTXList_columns_title_set(PyTX_Obj *self, PyObject *args) {
  PyObject *py_titles, *title;
  int i, ncols;
  char **titles;
  
  PyArg_ParseTuple(args, "O", &py_titles);

  ncols = PyList_Size(py_titles);
  titles = malloc(sizeof(char*) * ncols);
  assert(titles);

  for (i = 0; i < ncols; ++i) {
    title = PyList_GetItem(py_titles, i);
    titles[i] = strdup(PyString_AsString(title));
  }
  
  tx_list_columns_title_set(&(self->tx_obj), ncols, titles);
  free(titles);
  
  Py_INCREF(Py_None);
  Py_INCREF(py_titles);
	return Py_None;
}

static PyObject *PyTXList_scroll(PyTX_Obj *self, PyObject *args) {
  int scroll;

  PyArg_ParseTuple(args, "i", &scroll);
  tx_list_scroll(&(self->tx_obj), scroll);

  Py_INCREF(Py_None);
	return Py_None;
}

static PyObject *PyTXList_add(PyTX_Obj *self, PyObject *args) {
  int key;
  PyObject *obj;
  PyTXList_Funcs *funcs = self->tx_obj.data;

  PyArg_ParseTuple(args, funcs->text_cb?"iO":"is", &key, &obj);
  
  tx_list_row_sync(&(self->tx_obj), key, obj);
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTXList_remove(PyTX_Obj *self, PyObject *args) {
  int key;

  PyArg_ParseTuple(args, "i", &key);
  
  tx_list_row_remove(&(self->tx_obj), key);
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTXList_sort_set(PyTX_Obj *self, PyObject *args, PyObject *kwargs) {
  int col = -1;
  int asc = -1;
  char *kwlist[] = {"col", "asc", NULL };
  
  PyArg_ParseTupleAndKeywords(args, kwargs,"|ii", kwlist, &col, &asc);
  if (col != -1) tx_list_column_sort_set(&(self->tx_obj), col);
  if (asc != -1) tx_list_sort_asc_set(&(self->tx_obj), asc);
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTXList_sort_get(PyTX_Obj *self) {
  PyObject *result;
  int asc, col;
  
  col = tx_list_column_sort_get(&(self->tx_obj));
  asc = tx_list_sort_asc_get(&(self->tx_obj));

  result = PyTuple_Pack(2, PyInt_FromLong(col), PyInt_FromLong(asc));

  Py_INCREF(result);
	return result;
}

static PyObject *PyTXList_sort(PyTX_Obj *self, PyObject *args) {
  tx_list_sort(&(self->tx_obj));
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTXList_clear(PyTX_Obj *self) {
  tx_list_clear(&(self->tx_obj));
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTXList_sync(PyTX_Obj *self) {
  tx_list_sync(&(self->tx_obj));
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTXList_selected(PyTX_Obj *self, PyObject *args) {
  PyObject *result;
  int key;

  if (PyTuple_Size(args)) {
    PyArg_ParseTuple(args, "i", &key);
    tx_list_selected_set(&(self->tx_obj), key);
    Py_INCREF(Py_None);
	  return Py_None;
  }
  
  key = tx_list_selected_get(&(self->tx_obj));
  result = PyInt_FromLong(key);

  Py_INCREF(result);
	return result;
}

static PyMethodDef PyTXList_methods[] = {
  {"flag_set", (PyCFunction)(PyTXObj_flag_set), METH_VARARGS, NULL},
  {"flag_get", (PyCFunction)(PyTXObj_flag_get), METH_VARARGS, NULL},
  {"flag_toggle", (PyCFunction)(PyTXObj_flag_toggle), METH_VARARGS, NULL},
  {"draw", (PyCFunction)(PyTXObj_draw), METH_NOARGS, NULL},
  {"reset", (PyCFunction)(PyTXObj_reset), METH_NOARGS, NULL},
  {"add", (PyCFunction)(PyTXList_add), METH_VARARGS, NULL},
  {"remove", (PyCFunction)(PyTXList_remove), METH_VARARGS, NULL},
  {"scroll", (PyCFunction)(PyTXList_scroll), METH_VARARGS, NULL},
  {"selected", (PyCFunction)(PyTXList_selected), METH_VARARGS, NULL},
  {"sort", (PyCFunction)(PyTXList_sort), METH_NOARGS, NULL},
  {"sort_get", (PyCFunction)(PyTXList_sort_get), METH_NOARGS, NULL},
  {"sort_set", (PyCFunction)(PyTXList_sort_set), METH_VARARGS|METH_KEYWORDS, NULL},
  {"clear", (PyCFunction)(PyTXList_clear), METH_NOARGS, NULL},
  {"sync", (PyCFunction)(PyTXList_sync), METH_NOARGS, NULL},
  {"widths_set", (PyCFunction)(PyTXList_columns_width_set), METH_KEYWORDS, NULL},
  {"titles_set", (PyCFunction)(PyTXList_columns_title_set), METH_KEYWORDS, NULL},
  {NULL}  /* Sentinel */
};

static PyTypeObject PyTXList_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "pytx.List",               /*tp_name*/
    sizeof(PyTX_Obj),      /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)PyTXList_free, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    NULL,                /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    PyTXList_methods,             /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)PyTXList_init,      /* tp_init */
    0,                         /* tp_alloc */
    0,                       /* tp_new */
};

/* ---------------------------------------------------------- */

static void PyTXBox_free(PyTX_Obj* self) {
  self->ob_type->tp_free((PyObject*)self);
}

static int PyTXBox_init(PyTX_Obj *self, PyObject *args, PyObject *kwds) {
  int flags, align;
  /* FIXME: ensure we have selected VERT or HORZ */
  PyArg_ParseTuple(args, "i|i", &align, &flags);
  tx_obj_init(&(self->tx_obj), TX_BOX, PyTuple_Size(args) == 1 ? align : align|flags);
  return 0;
}

static PyObject *PyTXBox_add(PyTX_Obj *self, PyObject *args) {
  PyTX_Obj *add;
  PyArg_ParseTuple(args, "O", &add);

  tx_box_add(&(self->tx_obj), &(add->tx_obj)); 

  self->tx_obj.bind = self;
  tx_obj_getc_cb_set(&(self->tx_obj), getc_cb);

  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTXBox_adjust(PyTX_Obj *self) {
  tx_box_adjust(&(self->tx_obj)); 

  Py_INCREF(Py_None);
  return Py_None;
}



static PyMethodDef PyTXBox_methods[] = {
  {"add", (PyCFunction)(PyTXBox_add), METH_VARARGS, NULL},
  {"draw", (PyCFunction)(PyTXObj_draw), METH_NOARGS, NULL},
  {"adjust", (PyCFunction)(PyTXBox_adjust), METH_NOARGS, NULL},
  {NULL}  /* Sentinel */
};


static PyTypeObject PyTXBox_Type = {
    PyObject_HEAD_INIT(NULL)
    0,                         /*ob_size*/
    "pytx.Box",               /*tp_name*/
    sizeof(PyTX_Obj),      /*tp_basicsize*/
    0,                         /*tp_itemsize*/
    (destructor)PyTXBox_free, /*tp_dealloc*/
    0,                         /*tp_print*/
    0,                         /*tp_getattr*/
    0,                         /*tp_setattr*/
    0,                         /*tp_compare*/
    0,                         /*tp_repr*/
    0,                         /*tp_as_number*/
    0,                         /*tp_as_sequence*/
    0,                         /*tp_as_mapping*/
    0,                         /*tp_hash */
    0,                         /*tp_call*/
    0,                         /*tp_str*/
    0,                         /*tp_getattro*/
    0,                         /*tp_setattro*/
    0,                         /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
    NULL,                /* tp_doc */
    0,		               /* tp_traverse */
    0,		               /* tp_clear */
    0,		               /* tp_richcompare */
    0,		               /* tp_weaklistoffset */
    0,		               /* tp_iter */
    0,		               /* tp_iternext */
    PyTXBox_methods,             /* tp_methods */
    0,                         /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)PyTXBox_init,      /* tp_init */
    0,                         /* tp_alloc */
    0,                       /* tp_new */
};

/* ---------------------------------------------------------*/

static PyObject *PyTX_init(PyObject *self) {
  tx_init();
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTX_emergency(PyObject *self) {
  tx_ungetc(EMERGENCY);
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTX_free(PyObject *self) {
  tx_free();
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTX_adjust(PyTX_Obj *self) {
  tx_obj_map(tx_obj_root, tx_obj_resize);
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyObject *PyTX_operate(PyTX_Obj *self) {
  tx_obj_operate(tx_obj_focus);
  
  Py_INCREF(Py_None);
  return Py_None;
}

static PyMethodDef PyTX_functions[] = {
  {"init", (PyCFunction)(PyTX_init), METH_NOARGS, NULL},
  {"free", (PyCFunction)(PyTX_free), METH_NOARGS, NULL},
  {"operate", (PyCFunction)(PyTX_operate), METH_VARARGS, NULL},
  {"adjust", (PyCFunction)(PyTX_adjust), METH_VARARGS, NULL},
  {"emergency", (PyCFunction)(PyTX_emergency), METH_NOARGS, NULL},
  {NULL, NULL, 0, NULL }
};

DL_EXPORT(void) initpytx(void) {
  Py_InitModule("pytx", PyTX_functions);
  PyObject *m, *d;
  m = Py_InitModule3("pytx", PyTX_functions, NULL);

  PyTXList_Type.tp_new = PyType_GenericNew;
  if (PyType_Ready(&PyTXList_Type) < 0)return;
  Py_INCREF(&PyTXList_Type);
  PyModule_AddObject(m, "List", (PyObject *)&PyTXList_Type);
  
  PyTXBox_Type.tp_new = PyType_GenericNew;
  if (PyType_Ready(&PyTXBox_Type) < 0) return;
  Py_INCREF(&PyTXBox_Type);
  PyModule_AddObject(m, "Box", (PyObject *)&PyTXBox_Type);

  d = PyModule_GetDict(m);
  PyDict_SetItemString(d, "BORDER", PyInt_FromLong(TX_OBJ_BORDER));
  PyDict_SetItemString(d, "FOCUS", PyInt_FromLong(TX_OBJ_FOCUS));
  PyDict_SetItemString(d, "ROOT", PyInt_FromLong(TX_OBJ_ROOT));

  PyDict_SetItemString(d, "SCROLL", PyInt_FromLong(TX_LIST_SCROLL));
  PyDict_SetItemString(d, "SEPARATOR", PyInt_FromLong(TX_LIST_SEPARATOR));
  PyDict_SetItemString(d, "TITLES", PyInt_FromLong(TX_LIST_TITLE));
  PyDict_SetItemString(d, "HIGHLIGHT", PyInt_FromLong(TX_LIST_HLIGHT));
  
  PyDict_SetItemString(d, "VERT", PyInt_FromLong(TX_BOX_VERT));
  PyDict_SetItemString(d, "HORZ", PyInt_FromLong(TX_BOX_HORZ));

}
