#ifndef _TXOPS_H_
#define _TXOPS_H_
#include <ncurses.h>

#define EMERGENCY '`'
#define RESIZE -2

char *tx_text_blank(char *t, int w);
void tx_ungetc(int c);
void tx_stdwinset(WINDOW * win);
void tx_resize(void);
void tx_init(void);
void tx_free(void);
void tx_box(WINDOW *win);

#endif
