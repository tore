#include <panel.h>
#include <ncurses.h>
#include <assert.h>
#include <string.h>
#include "txops.h"
#include "txobj.h"
#include "txlist.h"
#include "txbox.h"

TXObj *tx_obj_focus;
TXObj *tx_obj_root;

static int getc_cb(TXObj *obj) {
  return wgetch(obj->win);
}

void tx_obj_size_cb_set(TXObj *obj, size_fn size_cb) {
  obj->size_cb = size_cb;
}

void tx_obj_idle_cb_set(TXObj *obj, obj_fn idle_cb) {
  obj->idle_cb = idle_cb;
}

void tx_obj_getc_cb_set(TXObj *obj, getc_fn getc_cb) {
  obj->getc_cb = getc_cb;
}

void tx_obj_rsze_cb_set(TXObj *obj, obj_fn rsze_cb) {
  obj->rsze_cb = rsze_cb;
}

/* apply cb to a box then each object in the box else apply cb to itself */
void tx_obj_map(TXObj *obj, obj_fn cb) {
  TXObj *entry;
  if (obj->type == TX_BOX) {
    cb(obj);
    TX_EXT(TXBox, box);
    list_for_each_entry(entry, &box->node, node) {
      tx_obj_map(entry, cb);
    }
    return;
  }
  cb(obj);
}

/* setup background window for border and static text, ie titles */
void tx_obj_setup(TXObj *obj) {
  if (obj->bwin) {
    wresize(obj->bwin, obj->h, obj->w);
    mvwin(obj->bwin, obj->y, obj->x);
    return;
  }
  obj->bwin = newwin(obj->h, obj->w, obj->y, obj->x);
  obj->bpanel = new_panel(obj->bwin);
}

void tx_obj_size_set(TXObj *obj, int x, int y, int w, int h) {
  obj->x = x; obj->y = y;
  obj->w = w; obj->h = h;
}

void tx_obj_border(TXObj *obj) {
  tx_box(obj->bwin);
  tx_obj_size_set(obj, obj->x+1, obj->y+1, obj->w-2, obj->h-2);
}

/* callback funcs */
void tx_obj_idle(TXObj *obj) {
  if (obj->idle_cb) obj->idle_cb(obj);
}

void tx_obj_draw(TXObj *obj) {
  obj->draw_cb(obj);
  update_panels();
  doupdate();
}

void tx_obj_resize(TXObj *obj) {
  if (obj->flags & TX_OBJ_ROOT) {
    tx_obj_size_set(obj, 0, 0, COLS, LINES);
    /* this allows the object to set the size if root is not in a box*/
    if (obj->type != TX_BOX) obj->size_cb(obj, obj->h, obj->w);
  }

  /* setup back window */
  tx_obj_setup(obj);
  /* draw border on back window */
  if (obj->flags & TX_OBJ_BORDER) tx_obj_border(obj);

  /* let object routines do whatever needs to be done */
  
  /* setup win object and draw on background, ie titles */
  if (obj->init_cb) obj->init_cb(obj);

  /* user defined routines, ie setup columns */
  if (obj->rsze_cb) obj->rsze_cb(obj);
}

void tx_obj_reset(TXObj *obj) {
  /* lets start fresh */
  getmaxyx(obj->bwin, obj->h, obj->w);
  getbegyx(obj->bwin, obj->y, obj->x);
  tx_obj_resize(obj);
}

/* object flag manipluation */
void tx_obj_flag_set(TXObj *obj, int flag, int set) {
  if (set) {
    obj->flags |= flag;
    return;
  }
  obj->flags &= ~flag;
}

int tx_obj_flag_get(TXObj *obj, int flag) {
  return obj->flags & flag;
}

int tx_obj_flag_toggle(TXObj *obj, int flag) {
  int set = (obj->flags & flag) ? 0 : 1;
  tx_obj_flag_set(obj, flag, set);
  return set;
}

void tx_obj_operate(TXObj *obj) {
  halfdelay(10);
  //timeout(0);
  int key;
  while (1) {
    switch(key = obj->getc_cb(obj)) {
      case 'q': return;
      case EMERGENCY:   tx_free(); return;
      case ERR:         tx_obj_map(tx_obj_root, tx_obj_idle); break;
      case KEY_RESIZE:  tx_obj_map(tx_obj_root, tx_obj_resize); break;
      default:          if (obj->oper_cb) obj->oper_cb(obj, key); break;
    }
  }
}

int tx_obj_init(TXObj *obj, int type, int flags) {
  memset(obj, 0, sizeof(TXObj));
  
  obj->flags = flags;
  obj->type  = type;
  
  obj->getc_cb = getc_cb;
  
  switch (type) {
    case TX_LIST: tx_list_new(obj); break;
    case TX_BOX:  tx_box_init(obj); break;
    default: return 0; break;
  }
  
  /* make inital object fill the screen */
  if (flags & TX_OBJ_ROOT) 
    tx_obj_root = obj;
  
  if (flags & TX_OBJ_FOCUS)
    tx_obj_focus = obj;
  
  return 1;
}
