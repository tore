#!/usr/bin/python
import pytx, traceback
from random import uniform
from time import sleep
from sys import stderr as err

def log(x):
  print >> err, str((x))

def test(f):
  def wrapper(*args, **kwargs):
    try: return f(*args, **kwargs)
    except: 
      traceback.print_exc()
      log("error: " + str(f))
  return wrapper

# FIXME: make fail nicely if text_cb are not defined
class Temp(pytx.List):
  def __init__(self, flags):
    pytx.List.__init__(self, flags)

  def text_cb(self, data):
    return [str(i) for i in data]
  
  @test
  def idle_cb(self, bla = False):
    if bla:
      for i in range(100):
        self.add(i, [chr(int(uniform(97,102))), "penny smells", i])
      self.sort()
      self.draw()
  
  @test
  def bind_cb(self, ch, key):
    if ch == 100: pass


    cols, ascd = self.sort_get()
    if 0 < ch-48 < len(self.widths) + 1:
      self.sort_set(ch-49, ascd)
    if ch-48 == 0:
      self.sort_set(cols, not ascd)
    self.sort()
    self.draw()
    self.draw()

  @test
  def rsze_cb(self):
    self.widths_set([5,15,5])
    self.titles_set(["chars", "strings", "ints"])

def test1():
  b1 = pytx.Box(pytx.ROOT|pytx.VERT|pytx.BORDER)
  b2 = pytx.Box(pytx.HORZ|pytx.BORDER)
  l1 = Temp(pytx.FOCUS|pytx.SCROLL|pytx.TITLES|pytx.HIGHLIGHT|pytx.SEPARATOR|pytx.BORDER)
  l2 = Temp(pytx.BORDER)
  l3 = Temp(pytx.BORDER)
  b1.add(l1)
  b1.add(b2)
  b2.add(l2)
  b2.add(l3)
  pytx.adjust()

  l1.idle_cb(bla=True)
  l2.idle_cb()
  
  pytx.operate()

if __name__ == "__main__":
  pytx.init()
  test1()
  pytx.free()
